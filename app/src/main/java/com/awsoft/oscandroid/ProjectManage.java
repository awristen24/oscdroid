package com.awsoft.oscandroid;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;

import com.awsoft.oscandroid.fragments.Fragments;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import static com.awsoft.oscandroid.OSCDroidMain.*;
import static com.awsoft.oscandroid.MenuItems.*;
import static com.awsoft.oscandroid.fragments.ProjectDetailsFragment.*;
import static com.awsoft.oscandroid.views.TrackGroupRecycler.*;

public class ProjectManage {

    Context projContext;

    public ProjectManage(Context context){
        projContext = context;

    }

    public void save(String filename)
    {
         XmlSerializer xmlSerializer = Xml.newSerializer();
            try {
                    xmlSerializer.setOutput(new FileWriter(filename));
                    xmlSerializer.startDocument(Xml.Encoding.UTF_8.name(), true);

                    xmlSerializer.startTag(null, "Project");
                    xmlSerializer.attribute(null, "name",           projectTitle.getText().toString());
                    xmlSerializer.attribute(null, "description",    projectDescription.getText().toString());

                    xmlSerializer.attribute(null, "artist",         projectInfo.get(ARTIST_INFO));
                    xmlSerializer.attribute(null, "album",          projectInfo.get(ALBUM_INFO));
                    xmlSerializer.attribute(null, "title",          projectInfo.get(TITLE_INFO));
                    xmlSerializer.attribute(null, "year",           projectInfo.get(YEAR_INFO));
                    xmlSerializer.attribute(null, "genre",          projectInfo.get(GENRE_INFO));
                    xmlSerializer.attribute(null, "copyright",      projectInfo.get(COPYRIGHT_INFO));
                    xmlSerializer.attribute(null, "runtime",        projectInfo.get(RUNTIME_INFO));
                    xmlSerializer.attribute(null, "date",           projectInfo.get(DATE_INFO));
                    xmlSerializer.attribute(null, "label",          projectInfo.get(LABEL_INFO));
                    xmlSerializer.attribute(null, "upc",            projectInfo.get(UPC_INFO));
                    xmlSerializer.attribute(null, "isrc",           projectInfo.get(ISRC_INFO));
                    xmlSerializer.attribute(null, "net",            projectInfo.get(NET_INFO));
                    xmlSerializer.attribute(null, "notes",          projectInfo.get(NOTES_INFO));
                    xmlSerializer.attribute(null, "datestamp", String.format("%d", System.currentTimeMillis()));
                    //xmlSerializer.attribute(null, "cover",          "cover.jpg");

                    if (OSCNetwork.IPADDRESS != null)
                        xmlSerializer.attribute(null, "ip", OSCNetwork.IPADDRESS.getHostAddress());
                    else
                        xmlSerializer.attribute(null, "ip", "");

                    if (OSCNetwork.PORT != 0)
                        xmlSerializer.attribute(null, "port", String.format("%d", OSCNetwork.PORT));
                    else
                        xmlSerializer.attribute(null, "port", "");

                xmlSerializer.startTag(null,"Transport");
                for (int j = 0;j < transportCtrlNum.size();j++) {
                    xmlSerializer.attribute(null, transportCtrlName.get(j) + "_ctrl",    Integer.toString(transportCtrlNum.get(j)));
                    xmlSerializer.attribute(null, transportCtrlName.get(j) + "_channel", Integer.toString(transportChNum.get(j)));
                }
                xmlSerializer.endTag(null,"Transport");

                for (int i = 0;i < LIST_ENTRIES;i++) {

                        xmlSerializer.startTag(null, "Index");
                        xmlSerializer.attribute(null, "number", Integer.toString(i));

                        xmlSerializer.startTag(null, "Track");
                        xmlSerializer.attribute(null, "label", trackState.get(i));
                        xmlSerializer.attribute(null, "color", String.format("#%08x", trackColor.get(i)));
                        xmlSerializer.endTag(null, "Track");

                        xmlSerializer.startTag(null, "Control");
                        xmlSerializer.attribute(null, "fader", Integer.toString(ctrlNum.get(i)));
                        xmlSerializer.attribute(null, "solo", Integer.toString(soloctrlNum.get(i)));
                        xmlSerializer.attribute(null, "mute", Integer.toString(mutectrlNum.get(i)));
                        xmlSerializer.endTag(null, "Control");

                        xmlSerializer.startTag(null, "Channel");
                        xmlSerializer.attribute(null, "fader", Integer.toString(channelNum.get(i)));
                        xmlSerializer.attribute(null, "solo", Integer.toString(solochannelNum.get(i)));
                        xmlSerializer.attribute(null, "mute", Integer.toString(mutechannelNum.get(i)));
                        xmlSerializer.endTag(null, "Channel");

                        xmlSerializer.startTag(null, "Value");
                        xmlSerializer.attribute(null, "fader", Integer.toString(ctrlValue.get(i)));
                        xmlSerializer.attribute(null, "solo", soloState.get(i).toString());
                        xmlSerializer.attribute(null, "mute", muteState.get(i).toString());
                        xmlSerializer.attribute(null, "min", minimumVal.get(i));
                        xmlSerializer.attribute(null, "max", maximumVal.get(i));
                        xmlSerializer.endTag(null, "Value");

                        xmlSerializer.startTag(null, "Tags");
                        xmlSerializer.attribute(null, "fader", osctag.get(i));
                        xmlSerializer.attribute(null, "solo", soloosctag.get(i));
                        xmlSerializer.attribute(null, "mute", muteosctag.get(i));
                        xmlSerializer.endTag(null, "Tags");

                     xmlSerializer.endTag(null, "Index");

                    }

                    xmlSerializer.endTag(null, "Project");
                    xmlSerializer.endDocument();

                } catch (IOException e) {
                    Toast.makeText(projContext, "Error saving " + filename, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    return;
                }

        Toast.makeText(projContext, "Saved " + filename, Toast.LENGTH_SHORT).show();
        valueDisplay.setText(filename + " saved");
    }

        int index_count = -1;
        FileReader fr;

        public void load(String filepath) {
            XmlPullParser xmlPullParser = Xml.newPullParser();

            Log.e("OSCDroid", "Load: " + filepath);
            try {
                fr = new FileReader(filepath);
                    xmlPullParser.setInput(fr);
            } catch (Exception e1) {
                Toast.makeText(projContext,"Bad XML file",Toast.LENGTH_SHORT).show();
                return;
            }
            try {
                xmlPullParser.next();       //Skip doc declaration, goto Project tag

                if (xmlPullParser.getName() == null) {
                    Toast.makeText(projContext,"Bad XML file",Toast.LENGTH_SHORT).show();
                    return;
                }

                projectTitle.setText(xmlPullParser.getAttributeValue(null, "name"));
                projectDescription.setText(xmlPullParser.getAttributeValue(null, "description"));

                projectInfo.set(ARTIST_INFO,    xmlPullParser.getAttributeValue(null, "artist"));
                projectInfo.set(ALBUM_INFO,     xmlPullParser.getAttributeValue(null, "album"));
                projectInfo.set(TITLE_INFO,     xmlPullParser.getAttributeValue(null, "title"));
                projectInfo.set(YEAR_INFO,      xmlPullParser.getAttributeValue(null, "year"));
                projectInfo.set(GENRE_INFO,     xmlPullParser.getAttributeValue(null, "genre"));
                projectInfo.set(COPYRIGHT_INFO, xmlPullParser.getAttributeValue(null, "copyright"));
                projectInfo.set(RUNTIME_INFO,   xmlPullParser.getAttributeValue(null, "runtime"));
                projectInfo.set(DATE_INFO,      xmlPullParser.getAttributeValue(null, "date"));
                projectInfo.set(LABEL_INFO,     xmlPullParser.getAttributeValue(null, "label"));
                projectInfo.set(UPC_INFO,       xmlPullParser.getAttributeValue(null, "upc"));
                projectInfo.set(ISRC_INFO,      xmlPullParser.getAttributeValue(null, "isrc"));
                projectInfo.set(NET_INFO,       xmlPullParser.getAttributeValue(null, "net"));
                projectInfo.set(NOTES_INFO,     xmlPullParser.getAttributeValue(null, "notes"));

                actv.setText(xmlPullParser.getAttributeValue(null, "ip"));
                porttext.setText(xmlPullParser.getAttributeValue(null, "port"));

                xmlPullParser.next();       //Transport tag


                for (int j = 0;j < 9;j++) {
                    int ctrl,ch;

                    try {
                        ctrl =  Integer.parseInt(xmlPullParser.getAttributeValue(null, transportCtrlName.get(j) + "_ctrl"));
                        ch =    Integer.parseInt(xmlPullParser.getAttributeValue(null, transportCtrlName.get(j) + "_channel"));
                    }
                    catch (NumberFormatException e)
                    {
                        Log.e("OSCDroid",String.format("Bad transport value for control index: %d",j));
                        break;
                    }
                    transportCtrlNum.set(j, ctrl);
                    transportChNum.set(j, ch);
                }

                xmlPullParser.next();       //Index tag

            } catch (XmlPullParserException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            clearData();

            try {
                do {
                    if (xmlPullParser.getName() != null) {
                        switch (xmlPullParser.getName()) {
                            case "Index":
                                if (xmlPullParser.getAttributeValue(null, "number") != null)
                                    index_count++;

                                break;

                            case "Track":
                                trackColor.add(index_count, Color.parseColor(xmlPullParser.getAttributeValue(null, "color")));
                                trackState.add(index_count, xmlPullParser.getAttributeValue(null, "label"));
                                break;

                            case "Control":
                                ctrlNum.add(index_count, Integer.parseInt(xmlPullParser.getAttributeValue(null, "fader")));
                                soloctrlNum.add(index_count, Integer.parseInt(xmlPullParser.getAttributeValue(null, "solo")));
                                mutectrlNum.add(index_count, Integer.parseInt(xmlPullParser.getAttributeValue(null, "mute")));
                                break;

                            case "Channel":
                                channelNum.add(index_count, Integer.parseInt(xmlPullParser.getAttributeValue(null, "fader")));
                                solochannelNum.add(index_count, Integer.parseInt(xmlPullParser.getAttributeValue(null, "solo")));
                                mutechannelNum.add(index_count, Integer.parseInt(xmlPullParser.getAttributeValue(null, "mute")));
                                break;

                            case "Value":
                                ctrlValue.add(index_count, Integer.parseInt(xmlPullParser.getAttributeValue(null, "fader")));
                                soloState.add(index_count, Boolean.parseBoolean(xmlPullParser.getAttributeValue(null, "solo")));
                                muteState.add(index_count, Boolean.parseBoolean(xmlPullParser.getAttributeValue(null, "mute")));

                                if (soloState.get(index_count))
                                    solo_count++;

                                minimumVal.add(index_count, xmlPullParser.getAttributeValue(null, "min"));
                                maximumVal.add(index_count, xmlPullParser.getAttributeValue(null, "max"));
                                break;

                            case "Tags":
                                osctag.add(index_count, xmlPullParser.getAttributeValue(null, "fader"));
                                soloosctag.add(index_count, xmlPullParser.getAttributeValue(null, "solo"));
                                muteosctag.add(index_count, xmlPullParser.getAttributeValue(null, "mute"));
                                break;

                        }
                    }
                    if (index_count > 5000) break;

                } while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT);
            } catch (XmlPullParserException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            LIST_ENTRIES = index_count + 1;
            valueDisplay.setText(String.format("Loaded %d tracks",LIST_ENTRIES));
            Fragments.trackGroup.notifyDataSetChanged();

        }

}


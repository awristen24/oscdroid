package com.awsoft.oscandroid;

import android.app.Activity;

import static com.awsoft.oscandroid.fragments.Fragments.*;
import static com.awsoft.oscandroid.views.TrackGroupRecycler.*;

public class Threads {

    public static Activity thread_activity;

    public Threads(Activity activity)
    {
        thread_activity = activity;
    }

    public Thread UpdateThread = new Thread(new Runnable(){
        @Override
        public void run() {
            thread_activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    trackGroup.notifyDataSetChanged();
                }
            });
        }
    });

    public Thread soloReset = new Thread(new Runnable() {
        @Override
        public void run() {

            for (int i = 0; i < soloState.size(); i++) {
                soloState.set(i, false);
                muteState.set(i, false);

                OSCNetwork.sendPacket(
                        OSCNetwork.IPADDRESS,
                        OSCNetwork.PORT,
                        muteosctag.get(i),
                        mutectrlNum.get(i),
                        ",iii",
                        0,
                        mutectrlNum.get(i),
                        mutechannelNum.get(i),
                        OSCNetwork.PACKET_SIZE
                );

            }
            solo_count = -1;
        }
    });


    public Thread InitOSCThread = new Thread(new Runnable(){

        @Override
        public void run() {
            for (int i = 0; i < LIST_ENTRIES; i++) {

                OSCNetwork.sendPacket(
                        OSCNetwork.IPADDRESS,
                        OSCNetwork.PORT,
                        osctag.get(i),
                        ctrlNum.get(i),
                        ",fii",
                        Float.floatToIntBits((float) ctrlValue.get(i) / 100),
                        ctrlNum.get(i),
                        channelNum.get(i),
                        OSCNetwork.PACKET_SIZE
                );

                    OSCNetwork.sendPacket(
                           OSCNetwork.IPADDRESS,
                           OSCNetwork.PORT,
                           muteosctag.get(i),
                           mutectrlNum.get(i),
                           ",iii",
                           booleanToInt(muteState.get(i)),
                           mutectrlNum.get(i),
                           mutechannelNum.get(i),
                           OSCNetwork.PACKET_SIZE
                     );

                OSCNetwork.sendPacket(
                        OSCNetwork.IPADDRESS,
                        OSCNetwork.PORT,
                        soloosctag.get(i),
                        soloctrlNum.get(i),
                        ",iii",
                        booleanToInt(soloState.get(i)),
                        soloctrlNum.get(i),
                        solochannelNum.get(i),
                        OSCNetwork.PACKET_SIZE
                );

            }
        }
    });
}

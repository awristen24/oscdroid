package com.awsoft.oscandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.awsoft.oscandroid.fragments.Fragments;
import com.awsoft.oscandroid.fragments.LoadProjectFragment;
import com.awsoft.oscandroid.fragments.ProjectDetailsFragment;
import com.awsoft.oscandroid.fragments.SaveProjectFragment;

import java.util.Random;

import static android.view.View.GONE;
import static com.awsoft.oscandroid.OSCDroidMain.*;
import static com.awsoft.oscandroid.fragments.ProjectDetailsFragment.*;
import static com.awsoft.oscandroid.fragments.SettingsFragment.*;
import static com.awsoft.oscandroid.views.TrackGroupRecycler.*;

public class MenuItems implements View.OnClickListener, Dialog.OnClickListener{

    public static Threads   threads;

    Context menucontext;

    public MenuItems(Context context)
    {
        menucontext = context;
        threads = new Threads((Activity)context);
    }

    public static int addTrack(int i)
    {
        if (i > MAX_TRACKS) return 1;

        ctrlValue.  add(DEFAULT_FADER_VALUE);
        int ch = Integer.parseInt(ch_edit.getText().toString());

        if (AUTO_COLOR) {
            Random rnd = new Random();
            trackColor.add(Color.argb(
                    rnd.nextInt(255),
                    rnd.nextInt(255),
                    rnd.nextInt(255),
                    rnd.nextInt(255)
            ));
        }
        else
            trackColor. add(0);

        trackState. add(String.format("%s %d", TRACK_LABEL_PREFIX, i + 1));
        osctag.     add(DEFAULT_TAG);
        channelNum. add(ch);
        ctrlNum.    add(i * 3 + 1);
        mutectrlNum.add(i * 3 + 2);
        soloctrlNum.add(i*3 + 3);
        soloosctag. add(DEFAULT_SOLO_TAG);
        soloState.  add(false);
        muteState.  add(false);
        muteosctag. add(DEFAULT_MUTE_TAG);
        minimumVal. add(DEFAULT_MIN);
        maximumVal. add(DEFAULT_MAX);
        solochannelNum.add(ch);
        mutechannelNum.add(ch);

        valueDisplay.setText(String.format("added track %d / ch: %d / ctrl: %d", i + 1, channelNum.get(i), i*3 + 1));

        return 0;
    }

    public static int createDefaults(int entries)
    {

        if (entries > MAX_TRACKS) return 1;
        for (int i = 0; i < entries; i++) {
            addTrack(i);
        }

        valueDisplay.setText(String.format("added %d tracks",entries));

        return 0;
    }

    public static void removeTrack(int i)
    {
        valueDisplay.setText("removed track");
        trackState.     remove(i);
        ctrlValue.      remove(i);
        soloState.      remove(i);
        trackColor.     remove(i);
        muteState.      remove(i);
        osctag.         remove(i);
        channelNum.     remove(i);
        ctrlNum.        remove(i);
        minimumVal.     remove(i);
        maximumVal.     remove(i);
        soloctrlNum.    remove(i);
        mutectrlNum.    remove(i);
        soloosctag.     remove(i);
        muteosctag.     remove(i);
        solochannelNum. remove(i);
        mutechannelNum. remove(i);

    }

    public static void clearData()
    {
        trackState.     clear();
        muteState.      clear();
        soloState.      clear();
        ctrlValue.      clear();
        trackColor.     clear();
        osctag.         clear();
        channelNum.     clear();
        ctrlNum.        clear();
        minimumVal.     clear();
        maximumVal.     clear();
        soloctrlNum.    clear();
        mutectrlNum.    clear();
        soloosctag.     clear();
        muteosctag.     clear();
        solochannelNum. clear();
        mutechannelNum. clear();
    }

    @Override
    public void onClick(View v) {
        Vibrator vibrator = (Vibrator) v.getContext().getSystemService(VIBRATOR_SERVICE);
        if (VIBRATE) vibrator.vibrate(70);

        switch (v.getId()) {
            case R.id.buttonNew:

                AlertDialog.Builder ab = new AlertDialog.Builder(v.getContext());
                ab.setTitle("New project");
                ab.setMessage("Save current project?");
                ab.setPositiveButton("Yes", this);
                ab.setNegativeButton("No", this);
                ab.setNeutralButton("Cancel", this);
                ab.setIcon(R.mipmap.app_icon_bw);

                ab.create().show();
                break;

            case R.id.buttonLoad:
                valueDisplay.setText("load");
                Fragments.loadFragment = new LoadProjectFragment();
                ((Activity)menucontext).getFragmentManager().beginTransaction().
                        add(android.R.id.content, Fragments.loadFragment).
                            addToBackStack(null).commit();

                MENU_TOGGLE = false;
                bottomControlMenu.setVisibility(GONE);
                break;

            case R.id.buttonSave:
                valueDisplay.setText("save");
                doSave(menucontext);
                break;

            case R.id.buttonAdd:

                addTrack(LIST_ENTRIES++);

                if (Fragments.trackGroup != null)
                    Fragments.trackGroup.notifyDataSetChanged();

                if (Fragments.listView != null)
                    Fragments.listView.smoothScrollToPosition(LIST_ENTRIES - 1);

                break;

            case R.id.buttonRemove:

                if (LIST_ENTRIES == 0) return;
                if (SELECTED_INDEX >= LIST_ENTRIES) SELECTED_INDEX--;

                Log.e("OSCDroid", String.format("selected index: %d", SELECTED_INDEX));

                LIST_ENTRIES--;
                removeTrack(SELECTED_INDEX);

                SELECTED_INDEX = index;

                if (Fragments.trackGroup != null)
                    Fragments.trackGroup.notifyDataSetChanged();

                break;

            case R.id.buttonInit:
                threads.InitOSCThread.run();
                valueDisplay.setText("OSC fader values sent");
                break;

        }

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which)
        {
            case DialogInterface.BUTTON_POSITIVE:

                SaveProjectFragment.START_NEW = true;
                doSave(menucontext);

                break;

            case DialogInterface.BUTTON_NEGATIVE:
                prepareNewProject();

                break;

        }

        dialog.dismiss();

    }

    public static void doSave(Context context)
    {
        if (projectTitle.getText().toString().isEmpty()) {
            Toast.makeText(context,"Please give this project a name before saving.",Toast.LENGTH_SHORT).show();
            valueDisplay.setText("Missing project name");
            projectTitle.requestFocus();
            return;
        }

        PROJECT_NAME = projectTitle.getText().toString();

        Fragments.saveFragment = new SaveProjectFragment();
        ((Activity)context).getFragmentManager().beginTransaction().
                add(android.R.id.content, Fragments.saveFragment).
                addToBackStack(null).commit();

        MENU_TOGGLE = false;
        bottomControlMenu.setVisibility(GONE);

    }

    public static void prepareNewProject()
    {
        valueDisplay.setText("new project started");
        clearData();

        artist_edit      .setText(null);
        album_edit       .setText(null);
        title_edit       .setText(null);
        year_edit        .setText(null);
        genre_edit       .setText(null);
        copyright_edit   .setText(null);
        runtime_edit     .setText(null);
        date_edit        .setText(null);
        label_edit       .setText(null);
        isrc_edit        .setText(null);
        upc_edit         .setText(null);
        net_edit         .setText(null);
        notes_edit       .setText(null);

        for (int i = 0;i < projectInfo.size();i++)
            projectInfo.set(i,null);

        createDefaults(NUM_TRACKS);

        LIST_ENTRIES = NUM_TRACKS;

        if (Fragments.trackGroup != null)
            Fragments.trackGroup.notifyDataSetChanged();

        ctrl_edit. setText(String.format("%d", ctrlNum.get(0)));
        ch_edit.   setText(String.format("%d", channelNum.get(0)));
        min_edit.  setText(minimumVal.get(0));
        max_edit.  setText(maximumVal.get(0));
        osctag_edit.    setText(osctag.get(0));
        trackLabel.setText(trackState.get(0));

        projectTitle.setText(null);
        projectDescription.setText(null);

        SaveProjectFragment.START_NEW = false;
    }

}

package com.awsoft.oscandroid.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.awsoft.oscandroid.MenuItems;
import com.awsoft.oscandroid.ProjectManage;
import com.awsoft.oscandroid.R;
import com.awsoft.oscandroid.fragments.Fragments;
import com.awsoft.oscandroid.fragments.ProjectDetailsFragment;
import com.awsoft.oscandroid.fragments.SaveProjectFragment;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;

import static com.awsoft.oscandroid.OSCDroidMain.*;
import static com.awsoft.oscandroid.fragments.SaveProjectFragment.*;
import static com.awsoft.oscandroid.fragments.SettingsFragment.*;

public class FileListAdapter extends BaseAdapter implements DialogInterface.OnClickListener{

    ArrayList<String>   dirnames =      new ArrayList<>();
    ArrayList<String>   filenames =     new ArrayList<>();
    ArrayList<File>     sorted_files =  new ArrayList<>();

    Context     fl_context;
    boolean     file_operation;

    public static File    root_dir;

    public FileListAdapter(Context context, String root, String savename, boolean operation)
    {
        if (root == null) return;

        fl_context = context;
        file_operation = operation;

        populate(root, operation);

    }

    @Override
    public int getCount() {
        return sorted_files.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        class ViewHolder {
            ImageView icon;
            TextView name;
        }

        ViewHolder vh;

        if (convertView == null) {
            convertView = LayoutInflater.from(fl_context).inflate(R.layout.folder_item,parent,false);

            vh = new ViewHolder();
            vh.icon = (ImageView) convertView.findViewById(R.id.folder_icon);
            vh.name = (TextView)  convertView.findViewById(R.id.folder_name);

            convertView.setTag(vh);
        }
        else
            vh = (ViewHolder)convertView.getTag();

            convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (VIBRATE) {
                    Vibrator vibrator = (Vibrator) v.getContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(70);
                }

                populate(sorted_files.get(position).getAbsolutePath(), file_operation);
                notifyDataSetChanged();

            }
        });

        if (sorted_files.get(position).isDirectory()) vh.icon.setImageResource(R.mipmap.folder_icon);
        else  vh.icon.setImageResource(R.mipmap.file_icon);

        if (sorted_files.get(position).isHidden()) vh.icon.setAlpha(0.5f);
        else  vh.icon.setAlpha(1.0f);

        vh.name.setText(sorted_files.get(position).getName());

        return convertView;
    }

    private void populate(String root, boolean load)
    {
        Log.e("OSCDroid", "open: " + root);

        filenames.      clear();
        dirnames.       clear();
        sorted_files.   clear();

        dirnames.add(root + "/.");
        dirnames.add(root + "/..");

        root_dir =  new File(root);
                Log.e("OSCDroid", "Path: " + root_dir.getAbsoluteFile().toString());

        File[]  files =     root_dir.listFiles(new FileFilter() {

            @Override
            public boolean accept(File pathname) {
                if (pathname.isFile() && pathname.getName().endsWith(".xml") || pathname.isDirectory()) return true;
                else return false;
            }
        });

        if (files == null) {

            if (root_dir.getAbsoluteFile().isFile()) {

                if (load) {
                    valueDisplay.setText("Loading " + root_dir.getAbsoluteFile().getName() + "...");

                    MenuItems.prepareNewProject();
                        new ProjectManage(fl_context).load(root_dir.getAbsoluteFile().toString());
                }
                else {
                    if (!root_dir.getAbsoluteFile().isFile()) {
                        valueDisplay.setText("Saving " + root_dir.getAbsoluteFile().getName() + "...");

                        String extension = ".xml";
                        File file = new File(root_dir.getAbsolutePath() + "/" + projectedit.getText().toString());

                        if (file.toString().endsWith(".xml")) extension = "";

                        //If exists already, prompt overwrite dialog
                        if (!projectedit.getText().toString().isEmpty())
                            new ProjectManage(fl_context).save(file.getAbsoluteFile().toString() + extension);
                        else
                            new ProjectManage(fl_context).save(root_dir.getAbsolutePath() + "/" + PROJECT_NAME + extension);
                    }
                    else
                    {
                        AlertDialog.Builder ab = new AlertDialog.Builder(fl_context);
                            ab.setIcon(R.mipmap.app_icon_bw);
                            ab.setMessage("Overwrite " + root_dir.getAbsoluteFile().getName() + "?");
                            ab.setTitle("Save");
                            ab.setPositiveButton("Yes", this);
                            ab.setNegativeButton("No", this);
                        ab.create().show();
                    }
                }

                bottomControlMenu.setVisibility(View.VISIBLE);

                if (SaveProjectFragment.START_NEW)
                    MenuItems.prepareNewProject();

                ((Activity)fl_context).getFragmentManager().beginTransaction().hide(Fragments.projectDetailsFragment).commit();
                ((Activity)fl_context).getFragmentManager().popBackStack();

                return;
            }

            sorted_files.add(new File("."));
            sorted_files.add(new File(".."));

            return;
        }

        for (int i = 0; i < files.length;i++) {
            if (files[i].isFile())
                    filenames.add(files[i].getAbsoluteFile().toString());
            else    dirnames.add(files[i].getAbsolutePath().toString());
        }

        //Sort alphabetically
        Collections.sort(filenames);
        Collections.sort(dirnames);

        for (int i = 0;i < dirnames.size();i++)  sorted_files.add(new File(dirnames.get(i)));
        for (int i = 0;i < filenames.size();i++) sorted_files.add(new File(filenames.get(i)));

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:

                new ProjectManage(fl_context).save(root_dir.getAbsoluteFile().toString());

                if (SaveProjectFragment.START_NEW)
                    MenuItems.prepareNewProject();

                break;

        }

        bottomControlMenu.setVisibility(View.VISIBLE);

    }
}

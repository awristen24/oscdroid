package com.awsoft.oscandroid.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.awsoft.oscandroid.R;

public class RecyclerViewHolder extends RecyclerView.ViewHolder {

    protected LinearLayout trackItem;
    protected TextView tv;
    protected SeekBar sb;
    protected ToggleButton mute, solo;

    public RecyclerViewHolder(View v) {
        super(v);

            trackItem = (LinearLayout)          v.findViewById(R.id.trackItem);
            tv =        (TextView)              v.findViewById(R.id.trackTitle);
            sb =        (SeekBar)               v.findViewById(R.id.trackFader);
            mute =      (ToggleButton)          v.findViewById(R.id.trackMute);
            solo =      (ToggleButton)          v.findViewById(R.id.trackSolo);

    }


}

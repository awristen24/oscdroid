package com.awsoft.oscandroid.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.awsoft.oscandroid.OSCNetwork;
import com.awsoft.oscandroid.R;

import static com.awsoft.oscandroid.OSCDroidMain.*;
import static com.awsoft.oscandroid.views.TrackGroupRecycler.*;

public class KnobView extends View {

    private Bitmap bmp, scaled_bmp;
    private float move_angle, x, y;
    private float progressval = 0;
    private float progress;
    private int data;

    public KnobView(Context context, AttributeSet attributeSet) {
        super(context);
        bmp = BitmapFactory.decodeResource(getResources(), R.drawable.knob);
    }

    @Override
    protected void onMeasure(int width, int height)
    {
        super.onMeasure(width,height);

        int KNOB_W = getMeasuredWidth()  + getPaddingLeft() + getPaddingRight();
        int KNOB_H = getMeasuredHeight() + getPaddingTop()  + getPaddingBottom();

        setMeasuredDimension(KNOB_W, KNOB_H);
        scaled_bmp = Bitmap.createScaledBitmap(bmp, KNOB_W, KNOB_H, true);

    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        canvas.rotate(move_angle, getWidth() / 2, getHeight() / 2);
        canvas.drawBitmap(scaled_bmp, 0, 0, null);

    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:

                ctrl_edit.  setText(String.format("%d", ctrlNum.get(SELECTED_INDEX)));
                ch_edit.    setText(String.format("%d", channelNum.get(SELECTED_INDEX)));
                min_edit.   setText(minimumVal.get(SELECTED_INDEX));
                max_edit.   setText(maximumVal.get(SELECTED_INDEX));
                osctag_edit.setText(osctag.get(SELECTED_INDEX));
                trackLabel. setText(trackState.get(SELECTED_INDEX));

                break;

            case MotionEvent.ACTION_MOVE:
                controlType = "knob";

                x = event.getX() / getWidth();
                y = event.getY() / getHeight();

                move_angle = (float) -Math.toDegrees(Math.atan2(1 - x - 0.5f, 1 - y - 0.5f));
                if (move_angle < 0) move_angle += 360;

                progress = move_angle / 360;
                if (progress >= 0.999f) {
                    Log.e("OSCDroid","Limit");
                    return true;
                }

                if (move_angle >= 0 && move_angle <= 360) {

                    progressval = progress * 100;
                    Log.e("OSCDroid", String.format("Value : %f : %d", progress, (int)progressval));

                    data = Float.floatToIntBits(progress);

                    ctrlValue.set(SELECTED_INDEX, (int)progressval);
                    valueDisplay.setText(String.format("%.3f / Ch: %d / Ctrl: %d",
                            progress, channelNum.get(SELECTED_INDEX), ctrlNum.get(SELECTED_INDEX)));

                    OSCNetwork.sendPacket(
                            OSCNetwork.IPADDRESS,
                            OSCNetwork.PORT,
                            osctag.get(SELECTED_INDEX),
                            ctrlNum.get(SELECTED_INDEX),
                            ",fii",
                            data,
                            ctrlNum.get(SELECTED_INDEX),
                            channelNum.get(SELECTED_INDEX),
                            OSCNetwork.PACKET_SIZE
                    );

                    invalidate();
                }

                break;

        }

        return true;
    }

}

package com.awsoft.oscandroid.views;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;

import static com.awsoft.oscandroid.OSCDroidMain.*;
import static com.awsoft.oscandroid.fragments.ColorFragment.*;

public class GradientView extends View {

    private static int ORIENTATION;
    private static Canvas
            colorbox_canvas, rainbowbox_canvas,greyscalebox_canvas;

    private static int  PAINTBOX_W, PAINTBOX_H,
                        RAINBOWBOX_W, RAINBOWBOX_H,
                        GREYSCALEBOX_W, GREYSCALEBOX_H;

    private static Bitmap colorBox, rainbowBox, greyscaleBox;
    private static LinearGradient colorBoxGradient, rainbowBoxGradient, greyscaleBoxGradient;

    public static int colorBox_colors[] = {   Color.BLACK, Color.RED     };
    private static int rainbow_colors[] =  {
            Color.RED,
            Color.YELLOW,
            Color.GREEN,
            Color.CYAN,
            Color.BLUE,
            Color.MAGENTA,
            Color.RED
    };

    private static int fiftyshadesofgrey[] = { 0,Color.BLACK,Color.DKGRAY,Color.LTGRAY,Color.WHITE,0 };
    Paint paint;

    public GradientView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();

        ORIENTATION = ((WindowManager)context.getSystemService(Context.WINDOW_SERVICE)).
                getDefaultDisplay().
                    getRotation();

    }

    Runnable updateRunnable = new Runnable(){
        @Override
        public void run(){
            invalidate();
            postDelayed(updateRunnable, 1000 / 15);
        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        ORIENTATION = newConfig.orientation;
    }

    @Override
        protected void onMeasure(int width, int height)
        {
            super.onMeasure(width, height);
            PAINTBOX_W = getPaddingLeft() + getPaddingRight() + getMeasuredWidth();
            PAINTBOX_H = getPaddingTop() + getPaddingBottom() + getMeasuredHeight();

            setMeasuredDimension(PAINTBOX_W, PAINTBOX_H);
        }

        @Override
        protected void onLayout(boolean changed, int l, int t, int r, int b)
        {
            super.onLayout(changed, l, t, r, b);

            colorBox = Bitmap.createBitmap(PAINTBOX_W, PAINTBOX_H, Bitmap.Config.ARGB_8888);
            colorBox.setHasAlpha(true);
            colorbox_canvas = new Canvas(colorBox);
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            colorBox.eraseColor(0);
                colorBoxGradient = new LinearGradient(0,PAINTBOX_H,PAINTBOX_W,0,colorBox_colors,null, Shader.TileMode.MIRROR);

                paint.setShader(colorBoxGradient);

                colorbox_canvas.drawRect(0, 0, PAINTBOX_W, PAINTBOX_H, paint);

            canvas.drawBitmap(colorBox, 0, 0, null);
            post(updateRunnable);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            super.onTouchEvent(event);
            int x = (int) event.getX(), y = (int) event.getY();
            if (x < PAINTBOX_W && y < PAINTBOX_H && x >= 0 && y >= 0) {
                SELECTED_COLOR = colorBox.getPixel(x, y);
                    valueDisplay.setText(String.format("Color: #%08x", SELECTED_COLOR));
                selectBox.setBackgroundColor(SELECTED_COLOR);
            }

            return true;
        }

    public static class RainbowGradientBar extends View {

        Paint paint;

        public RainbowGradientBar(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint = new Paint();

        }

        @Override
        protected void onMeasure(int width, int height)
        {
            super.onMeasure(width, height);
            RAINBOWBOX_W = getPaddingLeft() + getPaddingRight() + getMeasuredWidth();
            RAINBOWBOX_H = getPaddingTop() + getPaddingBottom() + getMeasuredHeight();

            setMeasuredDimension(RAINBOWBOX_W, RAINBOWBOX_H);
        }

        @Override
        protected void onLayout(boolean changed, int l, int t, int r, int b)
        {
            super.onLayout(changed, l, t, r, b);

            rainbowBox = Bitmap.createBitmap(RAINBOWBOX_W, RAINBOWBOX_H, Bitmap.Config.ARGB_8888);
            rainbowbox_canvas = new Canvas(rainbowBox);
        }

        @Override
        protected void onDraw(Canvas canvas)
        {

            if (ORIENTATION == Surface.ROTATION_0 || ORIENTATION == Surface.ROTATION_180)
                 rainbowBoxGradient = new LinearGradient(0,0,RAINBOWBOX_W,0,rainbow_colors,null, Shader.TileMode.MIRROR);
            else rainbowBoxGradient = new LinearGradient(0,0,0,RAINBOWBOX_H,rainbow_colors,null, Shader.TileMode.MIRROR);

            paint.setShader(rainbowBoxGradient);

            rainbowbox_canvas.drawRect(0, 0, RAINBOWBOX_W, RAINBOWBOX_H,paint);
            canvas.drawBitmap(rainbowBox,0,0,null);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            super.onTouchEvent(event);
            int x = (int) event.getX(), y = (int) event.getY();

            if (x < RAINBOWBOX_W && y < RAINBOWBOX_H && x >= 0 && y >= 0) {
                colorBox_colors[1] = rainbowBox.getPixel(x, y);
            }

            return true;
        }


    }

    public static class GreyscaleGradientBar extends View {

        Paint paint;
        public GreyscaleGradientBar(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint = new Paint();

        }

        @Override
        protected void onMeasure(int width, int height)
        {
            super.onMeasure(width, height);
            GREYSCALEBOX_W = getPaddingLeft() + getPaddingRight() + getMeasuredWidth();
            GREYSCALEBOX_H = getPaddingTop() + getPaddingBottom() + getMeasuredHeight();

            setMeasuredDimension(GREYSCALEBOX_W, GREYSCALEBOX_H);
        }

        @Override
        protected void onLayout(boolean changed, int l, int t, int r, int b)
        {
            super.onLayout(changed, l, t, r, b);

            greyscaleBox = Bitmap.createBitmap(GREYSCALEBOX_W, GREYSCALEBOX_H, Bitmap.Config.ARGB_8888);
            greyscaleBox.setHasAlpha(true);
            greyscalebox_canvas = new Canvas(greyscaleBox);
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            greyscaleBox.eraseColor(0);

            if (ORIENTATION == Surface.ROTATION_0 || ORIENTATION == Surface.ROTATION_180)
                 greyscaleBoxGradient = new LinearGradient(0,0,GREYSCALEBOX_W,0,fiftyshadesofgrey,null, Shader.TileMode.MIRROR);
            else greyscaleBoxGradient = new LinearGradient(0,GREYSCALEBOX_H,0,0,fiftyshadesofgrey,null, Shader.TileMode.MIRROR);

                paint.setShader(greyscaleBoxGradient);

            greyscalebox_canvas.drawRect(0,0,GREYSCALEBOX_W,GREYSCALEBOX_H,paint);

            canvas.drawBitmap(greyscaleBox, 0, 0, null);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            super.onTouchEvent(event);
            int x = (int) event.getX(), y = (int) event.getY();

            if (x < GREYSCALEBOX_W && y < GREYSCALEBOX_H && x >= 0 && y >= 0)
                colorBox_colors[0] = greyscaleBox.getPixel(x, y);

            return true;
        }

    }
}

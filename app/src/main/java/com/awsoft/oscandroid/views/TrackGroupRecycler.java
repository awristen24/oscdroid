package com.awsoft.oscandroid.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.awsoft.oscandroid.Listeners;
import com.awsoft.oscandroid.MenuItems;
import com.awsoft.oscandroid.R;
import com.awsoft.oscandroid.Threads;

import java.util.ArrayList;

import static com.awsoft.oscandroid.OSCDroidMain.valueDisplay;
import static com.awsoft.oscandroid.fragments.SettingsFragment.*;
import static com.awsoft.oscandroid.OSCDroidMain.SCREEN;

public class TrackGroupRecycler extends RecyclerView.Adapter<RecyclerViewHolder> {

    public static int       TRACK_GROUP = 1000;
    public static int       FADER_GROUP = 6000;
    public static int       KNOB_GROUP = 11000;
    public static int       MUTE_GROUP = 16000;
    public static int       SOLO_GROUP = 21000;
    public static int       TRACK_ITEM_GROUP = 26000;

    public static int       MAX_TRACKS = 5000;
    public static int       NUM_TRACKS = 3;

    public static ArrayList<Boolean>    muteState =     new ArrayList<>();
    public static ArrayList<Boolean>    soloState =     new ArrayList<>();
    public static ArrayList<Integer>    ctrlValue =     new ArrayList<>();
    public static ArrayList<String>     trackState =    new ArrayList<>();
    public static ArrayList<Integer>    trackColor =    new ArrayList<>();
    public static ArrayList<String>     osctag =        new ArrayList<>();
    public static ArrayList<Integer>    channelNum =    new ArrayList<>();
    public static ArrayList<Integer>    ctrlNum =       new ArrayList<>();
    public static ArrayList<Integer>    soloctrlNum =   new ArrayList<>();
    public static ArrayList<Integer>    mutectrlNum =   new ArrayList<>();
    public static ArrayList<Integer>   solochannelNum = new ArrayList<>();
    public static ArrayList<Integer>   mutechannelNum = new ArrayList<>();
    public static ArrayList<String>     soloosctag =    new ArrayList<>();
    public static ArrayList<String>     muteosctag =    new ArrayList<>();
    public static ArrayList<String>     minimumVal =    new ArrayList<>();
    public static ArrayList<String>     maximumVal =    new ArrayList<>();

    public static ArrayList<Integer>    transportChNum = new ArrayList<>();
    public static ArrayList<Integer>    transportCtrlNum = new ArrayList<>();
    public static ArrayList<String>    transportCtrlName = new ArrayList<>();

    public static int      LIST_ENTRIES;
    public static int      SELECTED_INDEX;
    public static int      index;

    public static int      solo_count;
    public static String   controlType = "none";

    private static  Context         trackGroupContext;

    private RecyclerViewHolder vh;
    private BitmapDrawable  thumb;
    private Bitmap          thumbbmp, newbmp;
    private Listeners listeners;

    public TrackGroupRecycler(Context context, int entries)
    {
        LIST_ENTRIES = entries;

        trackGroupContext =     context;

        MenuItems menuItems =   new MenuItems(context);
            if (menuItems.createDefaults(entries) == 1) {
                Toast.makeText(context, "Number of tracks cannot exceed 5000.", Toast.LENGTH_SHORT).show();
                valueDisplay.setText("Track limit reached");
            }
        listeners = new Listeners((Activity)trackGroupContext);

    }
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(trackGroupContext).inflate(R.layout.track_group, parent, false);
        vh = new RecyclerViewHolder(v);

            vh.solo.setOnLongClickListener(listeners.onLongClickListener);
            vh.solo.setOnClickListener(listeners.onSoloClick);
            vh.solo.setOnCheckedChangeListener(listeners.onSoloChecked);

            vh.mute.setOnLongClickListener(listeners.onLongClickListener);
            vh.mute.setOnClickListener(listeners.onMuteClick);
            vh.mute.setOnCheckedChangeListener(listeners.onMuteChecked);

            vh.tv.setOnClickListener(listeners.trackEditClick);
            vh.sb.setOnSeekBarChangeListener(listeners.seekBarListener);


        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder vh, final int position) {

        index = position;
        vh.solo.setId(SOLO_GROUP + position);
        vh.solo.setChecked(soloState.get(position));

        vh.mute.setId(MUTE_GROUP + position);
        vh.mute.setChecked(muteState.get(position));

        vh.trackItem.setId(TRACK_ITEM_GROUP + position);

        if (TRACK_HEIGHT.isEmpty()) TRACK_HEIGHT = "60";
        vh.trackItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Integer.parseInt(TRACK_HEIGHT)));

        if (position % 2 != 0)  vh.trackItem.setBackgroundColor(Color.rgb(0, 0, 0));
        else                    vh.trackItem.setBackgroundColor(Color.rgb(15, 15, 15));

        if (trackColor.get(position) != 0) vh.trackItem.setBackgroundColor(trackColor.get(position));

        vh.tv.setText(trackState.get(position));
        vh.tv.setId(TRACK_GROUP + position);

        vh.sb.setId(FADER_GROUP + position);
        vh.sb.setLayoutParams(new LinearLayout.LayoutParams(
                        SCREEN.width() - 250,
                        LinearLayout.LayoutParams.WRAP_CONTENT)
        );

        vh.sb.setRight(SCREEN.width() - 250);
        vh.sb.setMax((int) (Float.parseFloat(maximumVal.get(position)) - Float.parseFloat(minimumVal.get(position))) * 100);
        vh.sb.setProgress(ctrlValue.get(position));

        if (!FADER_COLOR)
            thumbbmp = BitmapFactory.decodeResource(trackGroupContext.getResources(), R.mipmap.hfader_black);
        else
            thumbbmp = BitmapFactory.decodeResource(trackGroupContext.getResources(), R.mipmap.hfader);

        newbmp = Bitmap.createScaledBitmap(thumbbmp,
                thumbbmp.getScaledWidth(DisplayMetrics.DENSITY_DEFAULT),
                Integer.parseInt(TRACK_HEIGHT), true);

        if (Integer.parseInt(TRACK_HEIGHT) > 0) {
            thumb = new BitmapDrawable(trackGroupContext.getResources(), newbmp);
        }
        else
            thumb = new BitmapDrawable(trackGroupContext.getResources(), thumbbmp);

        vh.sb.setMinimumHeight(Integer.parseInt(TRACK_HEIGHT));
        vh.sb.setThumb(thumb);

    }

    @Override
    public int getItemCount() {
        return LIST_ENTRIES;
    }

    public static Integer booleanToInt(boolean value)
    {
        if (value) return 1;
        else       return 0;
    }



}

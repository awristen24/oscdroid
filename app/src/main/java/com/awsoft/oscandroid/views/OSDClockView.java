package com.awsoft.oscandroid.views;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.awsoft.oscandroid.OSCNetwork;
import com.awsoft.oscandroid.R;

import java.text.SimpleDateFormat;

import static com.awsoft.oscandroid.OSCDroidMain.*;
import static com.awsoft.oscandroid.fragments.SettingsFragment.*;
import static com.awsoft.oscandroid.views.TrackGroupRecycler.*;

public class OSDClockView extends View  {

    private static int STOPPED = 0;
    private static int STARTED = 1;
    private static int PAUSED =  2;
    private static int RESUMED = 3;

    private static int state = 0;

    private int move_x = 0,  init_x = 0, offset_x = 0,
                move_y = 0,  init_y = 0, offset_y = 0;

    private int CLOCK_W, CLOCK_H,
        alpha = 200,
        shadow = 0x7700FFFF,
        pad_x = 30, pad_y = 20,
        textsize = 40;

    private static long
            hours = 0,
            milliseconds = 0,
            prevtime = 0,
            pause_time = 0;

    private static Vibrator vibrator;

    private static String clockdisplay = "00 : 00 : 00 . 000";
    private SimpleDateFormat milliFormat =  new SimpleDateFormat("SSS");
    private SimpleDateFormat secFormat =    new SimpleDateFormat("ss");
    private SimpleDateFormat minuteFormat = new SimpleDateFormat("mm");

    private String[] ctrlnames = {"Previous","Rewind","Play","Fast forward","Next","Record","Loop","Reset"};
    public static int TRANSPORT_INDEX;

    private Context knobContext;
    private Typeface lcdfont;
    private Paint paint, textpaint;
    private Rect bounds = new Rect();
    private SharedPreferences sp;

    private Runnable drawRunnable = new Runnable()
    {
        @Override
        public void run() {
            invalidate();
        }
    };

    private static void startTimer()
    {

        if (state == STOPPED) {
            valueDisplay.setText("started");
            prevtime = SystemClock.uptimeMillis();
            play_button.setImageResource(android.R.drawable.ic_media_pause);
            state = STARTED;
        }
        else if (state == STARTED || state == RESUMED){
            valueDisplay.setText("paused");
            pause_time = SystemClock.uptimeMillis();
            play_button.setImageResource(android.R.drawable.ic_media_play);
            state = PAUSED;
        }
        else if (state == PAUSED) {
            prevtime += SystemClock.uptimeMillis() - pause_time;
            valueDisplay.setText("resumed");
            play_button.setImageResource(android.R.drawable.ic_media_pause);
            state = RESUMED;
        }

    }

    public OSDClockView(Context context, AttributeSet attributeSet)
    {
        super(context);
        knobContext = context;

        sp = context.getSharedPreferences(context.getPackageName() + "_preferences", MODE_PRIVATE);
        vibrator = (Vibrator)context.getSystemService(VIBRATOR_SERVICE);
        lcdfont = Typeface.createFromAsset(context.getAssets(),"fonts/lcdbold.ttf");

        paint = new Paint();
        textpaint = new Paint();

        paint.setColor(Color.argb(alpha, 0, 0, 0));
        paint.setStyle(Paint.Style.FILL);

        textpaint.setTextSize(textsize);
        textpaint.setShadowLayer(6, 0, 0, shadow);
        textpaint.setColor(knobContext.getResources().getColor(android.R.color.holo_blue_bright));
        textpaint.setTypeface(lcdfont);

        for (int i = 0;i < 8;i++) {
            transportChNum.add(1);
            transportCtrlNum.add(120 + i);
            transportCtrlName.add(ctrlnames[i]);
        }

        move_x = sp.getInt("clock_x",0);
        move_y = sp.getInt("clock_y",0);

        offset_x = move_x;
        offset_y = move_y;

    }

    @Override
    protected void onMeasure(int width, int height)
    {
        super.onMeasure(width, height);

        textpaint.getTextBounds(clockdisplay, 0, clockdisplay.length(), bounds);

        CLOCK_W = bounds.width()  + getPaddingLeft() + getPaddingRight()  + pad_x;
        CLOCK_H = bounds.height() + getPaddingTop()  + getPaddingBottom() + pad_y;

        setMeasuredDimension(CLOCK_W, CLOCK_H);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        setLeft(move_x - init_x);
        setTop(move_y - init_y);
        setRight((move_x - init_x) + CLOCK_W);
        setBottom((move_y - init_y) + CLOCK_H);

    }

    @Override
    protected void onDraw(Canvas canvas) {

        if (state == STARTED || state == RESUMED)
            milliseconds = (SystemClock.uptimeMillis() - prevtime);
        else if (state == STOPPED) {
            milliseconds = 0;
            hours = 0;
        }

        clockdisplay = String.format("%02d : %s : %s . %s",
                hours,
                minuteFormat.format(milliseconds),
                secFormat.format(milliseconds),
                milliFormat.format(milliseconds)
        );

        if (Integer.parseInt(minuteFormat.format(milliseconds)) > 59)
            hours++;
        if (hours > 99)
            hours = 0;

        canvas.drawRoundRect(new RectF(0, 0, CLOCK_W, CLOCK_H), 5, 5, paint);
        canvas.drawText(clockdisplay, pad_x / 2, pad_y * 2, textpaint);

        postDelayed(drawRunnable, 1000 / 30);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:

                init_x = (int)event.getRawX() - offset_x;
                init_y = (int)event.getRawY() - offset_y;

                break;

            case MotionEvent.ACTION_UP:
                offset_x = move_x - init_x;
                offset_y = move_y - init_y;

                sp.edit().putInt("clock_x",offset_x).
                          putInt("clock_y",offset_y).
                          apply();

                break;

            case MotionEvent.ACTION_MOVE:
                move_x = (int)event.getRawX();
                move_y = (int)event.getRawY();

                if (move_x - init_x < SCREEN.width() - CLOCK_W && move_x - init_x > 0 &&
                        move_y - init_y < SCREEN.height() - CLOCK_H && move_y - init_y > 0 )
                layout(
                        move_x - init_x,
                        move_y - init_y,
                        (move_x - init_x) + CLOCK_W,
                        (move_y - init_y) + CLOCK_H
                );

                break;

        }

        return true;
    }

    public static View.OnClickListener transportClick = new OnClickListener() {

        @Override
        public void onClick(View v) {

            controlType = "transport";
            TRANSPORT_INDEX = Integer.parseInt((String) v.getTag());

            switch (v.getId())
            {
                case R.id.play_button:
                    startTimer();
                    break;

            }

            OSCNetwork.sendPacket(
                    OSCNetwork.IPADDRESS,
                    OSCNetwork.PORT,
                    DEFAULT_TRANSPORT_TAG,
                    transportCtrlNum.get(TRANSPORT_INDEX),
                    ",iii",
                    1,
                    transportCtrlNum.get(TRANSPORT_INDEX),
                    transportChNum.get(TRANSPORT_INDEX),
                    OSCNetwork.PACKET_SIZE
            );

            trackLabel. setText(v.getContentDescription());
            osctag_edit.setText(DEFAULT_TRANSPORT_TAG);
            ctrl_edit.  setText(String.format("%d", transportCtrlNum.get(TRANSPORT_INDEX)));
            ch_edit.    setText(String.format("%d", transportChNum.get(TRANSPORT_INDEX)));

            valueDisplay.setText(v.getContentDescription() + " : " + String.format("Ch: %d / Ctrl: %d",
                    transportChNum.get(TRANSPORT_INDEX),transportCtrlNum.get(TRANSPORT_INDEX)));

        }
    };

    public static View.OnLongClickListener transportReset = new OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            if (VIBRATE) vibrator.vibrate(70);

            TRANSPORT_INDEX = 7; //reset
            state = STOPPED;
            clockdisplay = "00 : 00 : 00 . 000";

            valueDisplay.setText("reset");

            OSCNetwork.sendPacket(
                    OSCNetwork.IPADDRESS,
                    OSCNetwork.PORT,
                    DEFAULT_TRANSPORT_TAG,
                    transportCtrlNum.get(TRANSPORT_INDEX),
                    ",iii",
                    1,
                    transportCtrlNum.get(TRANSPORT_INDEX),
                    transportChNum.get(TRANSPORT_INDEX),
                    OSCNetwork.PACKET_SIZE
            );

            trackLabel. setText("Reset");
            osctag_edit.setText(DEFAULT_TRANSPORT_TAG);
            ctrl_edit.  setText(String.format("%d", transportCtrlNum.get(TRANSPORT_INDEX)));
            ch_edit.    setText(String.format("%d", transportChNum.get(TRANSPORT_INDEX)));

            valueDisplay.setText("Reset : " + String.format("Ch: %d / Ctrl: %d",
                    transportChNum.get(TRANSPORT_INDEX),transportCtrlNum.get(TRANSPORT_INDEX)));

            return true;
        }
    };

}

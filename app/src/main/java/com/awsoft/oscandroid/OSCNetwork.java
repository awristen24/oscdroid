package com.awsoft.oscandroid;

import android.os.AsyncTask;
import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;

import static com.awsoft.oscandroid.OSCDroidMain.*;

public class OSCNetwork {

    public static DatagramSocket  socket;
    public static InetAddress     IPADDRESS;

    public static int PACKET_SIZE = 40;
    public static int AUTO_SIZE = -1;
    public static int PORT;

    public static void init()
    {
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }

    }

    public static void sendPacket(
            InetAddress ipaddr,
            int port,
            String tag,
            int oscctl,
            String format,
            Integer data,
            int midi_cc,
            int midi_ch,
            int packet_len)
    {

        if (socket == null)
            return;

        if (packet_len % 4 != 0 && packet_len != AUTO_SIZE) {
            Log.e("OSCDroid", "Packet size not a multiple of 4");
            return;
        }

        byte chunk[] = {
                (byte)((data & 0xFF000000) >> 24),
                (byte)((data & 0xFF0000) >> 16),
                (byte)((data & 0xFF00) >> 8),
                (byte)(data & 0xFF),
                0,0,0,(byte)midi_cc,
                0,0,0,(byte)midi_ch
        };

        ByteBuffer byteBuffer = null;
        int len = 0, newlen = 0;

        if (packet_len != AUTO_SIZE) {
            byteBuffer = ByteBuffer.allocate(packet_len);
            byteBuffer.put(String.format("%s%d", tag, oscctl).getBytes());
        }

        //Auto size flag on will retrieve data length
        int data_len = setData(packet_len, byteBuffer, tag, oscctl, format, chunk);

        if (packet_len == AUTO_SIZE) {
            byteBuffer = ByteBuffer.allocate(data_len);
            byteBuffer.put(String.format("%s%d", tag, oscctl).getBytes());

            Log.e("OSCDroid",String.format("Data len: %d",data_len));

            if (data_len % 4 != 0) {
                Log.e("OSCDroid", "Packet size not a multiple of 4");
                return;
            }

            setData(data_len, byteBuffer, tag, oscctl, format, chunk);

        }

        packet_len = byteBuffer.array().length;

        final DatagramPacket packet = new DatagramPacket(byteBuffer.array(), packet_len);
                packet.setAddress(ipaddr);
                packet.setPort(port);

        Log.e("OSCDroid",String.format("Sent packet over %s:%d",ipaddr,port));

        new NetworkTask().execute(packet);

    }

    public static int setData(int packet_len, ByteBuffer byteBuffer, String tag, int oscctl, String format, byte[] chunk)
    {
        int len = String.format("%s%d",tag,oscctl).length();
        int newlen = 0;

        for (int i = 1;i < len;i++) {
            if (packet_len != AUTO_SIZE)
                byteBuffer.put((byte) 0x0);

            newlen = i;
            if ((len + i) % 4 == 0) break;
        }

        if (packet_len != AUTO_SIZE)
            byteBuffer.put(format.getBytes());

        for (int i = 0;i < 4;i++) {
            if (packet_len != AUTO_SIZE)
                byteBuffer.put((byte) 0x0);
        }

        if (packet_len != AUTO_SIZE)
            byteBuffer.put(chunk);

        return len + newlen + format.length() + 4 + chunk.length;
    }

    public static class NetworkTask extends AsyncTask<Object,Integer,Integer> {
        int err = 0;

        @Override
        protected Integer doInBackground(Object[] params) {
            try {
                socket.send((DatagramPacket)params[0]);
            } catch (Exception e) {
                err = 1;
                e.printStackTrace();
            }

            publishProgress(err);
            return err;
        }

        @Override
        protected void onProgressUpdate(Integer...progress)
        {
            if (err == 1) valueDisplay.setText("Send error! Check IP/Port");

        }
    };
}

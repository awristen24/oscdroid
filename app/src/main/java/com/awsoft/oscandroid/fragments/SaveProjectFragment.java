package com.awsoft.oscandroid.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.awsoft.oscandroid.ProjectManage;
import com.awsoft.oscandroid.R;
import com.awsoft.oscandroid.views.FileListAdapter;

import java.io.File;

import static com.awsoft.oscandroid.fragments.SettingsFragment.*;

public class SaveProjectFragment extends Fragment {

    public static EditText projectedit;
    public static boolean  START_NEW;
    Button projectButton;

        @Override
        public View onCreateView(LayoutInflater li, ViewGroup container, Bundle icicle)
        {
            super.onCreateView(li, container, icicle);
            View v = li.inflate(R.layout.save_layout,container,false);

            FileListAdapter fla = new FileListAdapter(v.getContext(),
                    Environment.getExternalStorageDirectory().toString(),
                        PROJECT_NAME + ".xml", false);

            projectedit = (EditText)v.findViewById(R.id.saveProjectName);
                projectedit.setText(PROJECT_NAME + ".xml");

            projectButton = (Button)v.findViewById(R.id.buttonProjectSave);
            projectButton.setBackgroundResource(R.drawable.roundrect);
            projectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String extension = ".xml";
                    File file = new File(FileListAdapter.root_dir.getAbsolutePath() + "/" + projectedit.getText().toString());
                         if (file.toString().endsWith(".xml")) extension = "";

                    new ProjectManage(getActivity()).save(file.getAbsoluteFile().toString() + extension);
                        getFragmentManager().popBackStack();
                }
            });

            ListView filelist = (ListView)v.findViewById(R.id.file_list);
                filelist.setAdapter(fla);

            return v;
        }

}

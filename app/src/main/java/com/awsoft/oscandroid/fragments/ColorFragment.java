package com.awsoft.oscandroid.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.awsoft.oscandroid.R;

import static com.awsoft.oscandroid.OSCDroidMain.colorSwatch;
import static com.awsoft.oscandroid.fragments.Fragments.*;
import static com.awsoft.oscandroid.views.TrackGroupRecycler.SELECTED_INDEX;
import static com.awsoft.oscandroid.views.TrackGroupRecycler.trackColor;

public class ColorFragment extends Fragment implements DialogInterface.OnClickListener
{
    public static int  SCREEN_ORIENTATION, SELECTED_COLOR;
    public static FrameLayout selectBox;
    private static AlertDialog ad;

    private AlertDialog createDialog()
    {
        View colorview;
        LayoutInflater li = LayoutInflater.from(getActivity());

        SCREEN_ORIENTATION = ((WindowManager)getActivity().getSystemService(Context.WINDOW_SERVICE)).
                getDefaultDisplay().
                    getRotation();

        if (SCREEN_ORIENTATION == Surface.ROTATION_0 || SCREEN_ORIENTATION == Surface.ROTATION_180) {
            colorview = li.inflate(R.layout.color_dialog, null, false);
        }
        else {
            colorview = li.inflate(R.layout.color_dialog_landscape, null, false);
        }

        selectBox = (FrameLayout)colorview.findViewById(R.id.selectedColor);

        AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
            ab.setView(colorview);
            ab.setPositiveButton("Choose", this);
            ab.setNeutralButton("Clear color", this);
            ab.setNegativeButton("Cancel", this);
        ad = ab.create();

        return ad;
    }
    @Override
    public void onConfigurationChanged(Configuration newconfig)
    {
        if (ad != null) ad.dismiss();

        ad = createDialog();
        ad.show();
    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup container, Bundle icicle)
    {
        if (ad != null) ad.dismiss();

        ad = createDialog();
        ad.show();

        return null;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which)
        {
            case DialogInterface.BUTTON_POSITIVE:

                trackColor.set(SELECTED_INDEX, SELECTED_COLOR);
                selectBox.setBackgroundColor(SELECTED_COLOR);
                colorSwatch.setBackgroundColor(SELECTED_COLOR);
                trackGroup.notifyDataSetChanged();

                break;

            case DialogInterface.BUTTON_NEUTRAL:

                trackColor.set(SELECTED_INDEX, 0);
                selectBox.setBackgroundColor(SELECTED_COLOR);
                colorSwatch.setBackgroundColor(SELECTED_COLOR);
                trackGroup.notifyDataSetChanged();

                break;

        }

        getFragmentManager().popBackStack();
    }
}
package com.awsoft.oscandroid.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.awsoft.oscandroid.R;
import com.awsoft.oscandroid.views.TrackGroupRecycler;

import static com.awsoft.oscandroid.fragments.Fragments.*;
import static com.awsoft.oscandroid.OSCDroidMain.*;
import static com.awsoft.oscandroid.fragments.SettingsFragment.*;
import static com.awsoft.oscandroid.views.TrackGroupRecycler.*;

public class MixFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle icicle) {
        super.onCreateView(inflater, container, icicle);

        SharedPreferences sp = getActivity().getSharedPreferences(
                getActivity().getPackageName() + "_preferences",
                    Context.MODE_PRIVATE
        );

        View v = inflater.inflate(R.layout.mix_layout, container, false);

        readSharedPreferences(getActivity(), sp);
        trackGroup = new TrackGroupRecycler(getActivity(), NUM_TRACKS);

        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
            lm.setOrientation(LinearLayoutManager.VERTICAL);
            lm.setStackFromEnd(true);
            lm.setSmoothScrollbarEnabled(true);

        listView = (RecyclerView)v.findViewById(R.id.listView);
            listView.setLayoutManager(lm);

        if (LIST_TRANSPARENCY)  listView.setAlpha(0.8f);
        else                    listView.setAlpha(1);

        listView.setAdapter(trackGroup);

        ctrl_edit.  setText(String.format("%d", ctrlNum.get(0)));
        ch_edit.    setText(String.format("%d", channelNum.get(0)));
        min_edit.   setText(minimumVal.get(0));
        max_edit.   setText(maximumVal.get(0));
        osctag_edit.setText(osctag.get(0));
        trackLabel. setText(trackState.get(0));

        return v;
    }

}

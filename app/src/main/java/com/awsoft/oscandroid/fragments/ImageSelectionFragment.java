package com.awsoft.oscandroid.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.awsoft.oscandroid.R;

public class ImageSelectionFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup container, Bundle icicle)
    {
        super.onCreateView(li, container, icicle);
        View view = li.inflate(R.layout.image_selector,container,false);

        return view;
    }
}

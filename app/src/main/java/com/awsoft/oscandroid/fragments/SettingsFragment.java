package com.awsoft.oscandroid.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.awsoft.oscandroid.OSCNetwork;
import com.awsoft.oscandroid.R;
import com.awsoft.oscandroid.Threads;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import static android.content.Context.MODE_PRIVATE;
import static com.awsoft.oscandroid.fragments.Fragments.*;
import static com.awsoft.oscandroid.OSCDroidMain.*;
import static com.awsoft.oscandroid.views.TrackGroupRecycler.*;

public class SettingsFragment extends PreferenceFragment implements
        SharedPreferences.OnSharedPreferenceChangeListener,DialogInterface.OnClickListener
{
    public static String    DEFAULT_TAG         = "/fader/";
    public static String    DEFAULT_MUTE_TAG    = "/mute/";
    public static String    DEFAULT_SOLO_TAG    = "/solo/";
    public static String    DEFAULT_TRANSPORT_TAG = "/transport/";
    public static String    DEFAULT_MIN         = "0.0";
    public static String    DEFAULT_MAX         = "1.0";
    public static int       DEFAULT_FADER_VALUE = 50;
    public static String    TRACK_HEIGHT        = "60";
    public static String    TRACK_LABEL_PREFIX  = "Track";

    public static boolean VIBRATE           = false;
    public static boolean AUTO_COLOR        = false;
    public static boolean MENU_TOGGLE       = true;
    public static boolean CLOCK_TOGGLE      = false;
    public static boolean FADER_COLOR       = false;
    public static boolean LIST_TRANSPARENCY = true;
    public static String  PROJECT_NAME      = "Untitled";

    private static SharedPreferences.Editor editor;
    private static SharedPreferences sp;

    public static class TransparencyFragment extends Fragment
    {
        @Override
        public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle icicle)
        {
            float level = sp.getFloat("listTransparency",0.88f) * 255;

            SeekBar seekbar = new SeekBar(getActivity());
                seekbar.setMax(255);
                seekbar.setBackgroundColor(Color.argb(200, 0, 0, 0));
                seekbar.setProgress((int)level);
                seekbar.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    valueDisplay.setText(String.format("Level: %d", progress));
                        listView.setAlpha((float) progress / 255);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getFragmentManager().beginTransaction().remove(transparencyFragment).commit();
                        }
                    }, 2000);
                    editor.putFloat("listTransparency", listView.getAlpha()).apply();

                }
            });

            LinearLayout ll = new LinearLayout(getActivity());

                ll.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                ll.setGravity(Gravity.TOP);
                ll.addView(seekbar);

            return ll;
        }

    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle icicle) {
        View view = super.onCreateView(li, vg, icicle);

        if (view != null)
            view.setBackgroundColor(Color.argb(220, 0, 0, 0));

        return view;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        PreferenceManager pm = getPreferenceManager();
            sp = pm.getSharedPreferences();
                editor = sp.edit();
            sp.registerOnSharedPreferenceChangeListener(this);

        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.e("OSCDroid", "onSharedPreferenceChanged");
        readSharedPreferences(getActivity(), sharedPreferences);    //read all

        switch (key)
        {
            case "autoColor":
                for (int i = 0;i < LIST_ENTRIES;i++) {
                    if (AUTO_COLOR) {
                        Random rnd = new Random();
                        trackColor.set(i, Color.argb(
                                rnd.nextInt(255),
                                rnd.nextInt(255),
                                rnd.nextInt(255),
                                rnd.nextInt(255)
                        ));
                    } else
                        trackColor.set(i, 0);
                }

                trackGroup.notifyDataSetChanged();
                break;

            case "toggleListTransparency":
                if (LIST_TRANSPARENCY)  listView.setAlpha(sp.getFloat("listTransparency",0.88f));
                else                    listView.setAlpha(1);
                break;

        }
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen ps, Preference preference)  {
        super.onPreferenceTreeClick(ps, preference);

        switch (preference.getKey()) {

            case "listTransparency":

                getFragmentManager().popBackStack();
                transparencyFragment = new TransparencyFragment();
                getFragmentManager().
                        beginTransaction().
                            add(android.R.id.content, transparencyFragment).
                                commit();

                break;

            case "resetToDefault":

                AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
                    ab.setTitle("Reset");
                    ab.setMessage("Confirm revert settings to default?");
                    ab.setPositiveButton("Yes", this);
                    ab.setNegativeButton("No", this);
                ab.create().show();

                break;

            case "resetClockPosition":
                sp.edit().putInt("clock_x", 0).putInt("clock_y", 0).apply();

                getFragmentManager().beginTransaction().detach(Fragments.osdClock).
                        attach(Fragments.osdClock).
                            detach(Fragments.settingsFragment).
                                attach(Fragments.settingsFragment).commit();

                break;

            case "getPDfile":

                final File file = new File(Environment.getExternalStorageDirectory() + "/osc_midi_bridge.pd");
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            InputStream is = getActivity().getAssets().open("pd/osc_midi_bridge.pd", AssetManager.ACCESS_BUFFER);
                            FileOutputStream fos = new FileOutputStream(file);

                            int read = 0;

                            do {
                                read = is.read();
                                if (read != -1) fos.write(read);
                            } while (read != -1);

                            is.close();
                            fos.close();

                        } catch (IOException e) {
                            Toast.makeText(getActivity(),
                                    "Could not read/write asset file: " +
                                            file.getName(), Toast.LENGTH_SHORT).show();

                            e.printStackTrace();
                            return;
                        }
                    }
                });

                DownloadManager dm = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
                    dm.addCompletedDownload(file.getName(),
                            "OSCDroid",
                            true,
                            "text/plain",
                            file.getAbsoluteFile().toString(),
                            file.length(),
                            true
                    );

                Toast.makeText(getActivity(),
                        "Asset file written to " + file.getAbsoluteFile().toString(),
                            Toast.LENGTH_SHORT).show();

                break;

            case "sendAll":
                new Threads(getActivity()).InitOSCThread.run();
                break;

            case "resetFaders":
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < LIST_ENTRIES; i++) {
                            ctrlValue.set(i, 50);

                            OSCNetwork.sendPacket(
                                    OSCNetwork.IPADDRESS,
                                    OSCNetwork.PORT,
                                    osctag.get(i),
                                    ctrlNum.get(i),
                                    ",fii",
                                    Float.floatToIntBits(0.50f),
                                    ctrlNum.get(i),
                                    channelNum.get(i),
                                    OSCNetwork.PACKET_SIZE
                            );
                        }

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                trackGroup.notifyDataSetChanged();
                            }
                        });

                    }
                });

                break;
        }

        return true;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        switch (which)
        {
            case DialogInterface.BUTTON_POSITIVE:

                SharedPreferences sharedPreferences = getActivity().
                        getSharedPreferences(getActivity().
                                getPackageName() + "_preferences",MODE_PRIVATE);

                SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("immersiveMode", false);
                    editor.putBoolean("showDataBar", true);
                    editor.putBoolean("autoColor", false);
                    editor.putBoolean("haptic", false);
                    editor.putBoolean("toggleListTransparency", true);
                    editor.putFloat("listTransparency", 0.88f);
                    editor.putString("numtracks", "3");
                    editor.putString("defaultSoloTag", "/solo/");
                    editor.putString("defaultMuteTag", "/mute/");
                    editor.putString("defaultFaderTag", "/fader/");
                    editor.putString("defaultTransportTag", "/transport/");
                    editor.putString("defaultKnobTag", "/knokb/");
                    editor.putString("trackPrefix", "Track");
                    editor.putBoolean("lightfaders", false);
                    editor.putString("trackHeight", "60");
                    editor.putString("customPacketSize", "40");
                    editor.putBoolean("autoPacketSize", true);
                    editor.putString("defaultMinimum","0.0");
                    editor.putString("defaultMaximum","1.0");

                    editor.apply();

                listView.setAlpha(0.8f);

                for (int i = 0;i < LIST_ENTRIES;i++)
                    trackColor.set(i, 0);

                readSharedPreferences(getActivity(), sharedPreferences);

                setPreferenceScreen(null);
                addPreferencesFromResource(R.xml.preferences);

                break;

        }
    }
    public static void readSharedPreferences(Activity activity, SharedPreferences sharedPreferences)
    {
        Window window = null;
        if (activity != null)
            window = activity.getWindow();

        if (window != null) {
            if (sharedPreferences.getBoolean("immersiveMode", false))
                window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            else if (!sharedPreferences.getBoolean("immersiveMode", false))
                window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        if (sharedPreferences.getBoolean("showDataBar",true))
            displayBar.setVisibility(View.VISIBLE);
        else if (!sharedPreferences.getBoolean("showDataBar",true))
            displayBar.setVisibility(View.GONE);

        OSCNetwork.PACKET_SIZE = Integer.parseInt(sharedPreferences.getString("customPacketSize", "40"));
        if (sharedPreferences.getBoolean("autoPacketSize",true))
            OSCNetwork.PACKET_SIZE = OSCNetwork.AUTO_SIZE;

        AUTO_COLOR =        sharedPreferences.getBoolean("autoColor", false);
        VIBRATE =           sharedPreferences.getBoolean("haptic", false);
        LIST_TRANSPARENCY = sharedPreferences.getBoolean("toggleListTransparency", true);
        TRACK_HEIGHT =      sharedPreferences.getString("trackHeight", "60");

        try {
            NUM_TRACKS = Integer.parseInt(sharedPreferences.getString("numtracks", "3"));
            if (Integer.parseInt(TRACK_HEIGHT) <= 0) TRACK_HEIGHT = "60";
        }
        catch (Exception e)
        {
            //
        }

        DEFAULT_SOLO_TAG =      sharedPreferences.getString("defaultSoloTag",   "/solo/");
        DEFAULT_MUTE_TAG =      sharedPreferences.getString("defaultMuteTag",   "/mute/");
        DEFAULT_TAG =           sharedPreferences.getString("defaultFaderTag",  "/fader/");
        DEFAULT_TRANSPORT_TAG = sharedPreferences.getString("defaultTransportTag", "/transport/");
        DEFAULT_MIN          =  sharedPreferences.getString("defaultMinimum",   "0.0");
        DEFAULT_MAX          =  sharedPreferences.getString("defaultMaximum",   "1.0");
        TRACK_LABEL_PREFIX =    sharedPreferences.getString("trackPrefix",      "Track");
        FADER_COLOR =           sharedPreferences.getBoolean("lightfaders", false);

        if (Fragments.trackGroup != null) Fragments.trackGroup.notifyDataSetChanged();
    }

}
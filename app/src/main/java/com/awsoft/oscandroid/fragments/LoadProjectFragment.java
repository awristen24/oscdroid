package com.awsoft.oscandroid.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.awsoft.oscandroid.R;
import com.awsoft.oscandroid.views.*;

public class LoadProjectFragment extends Fragment
{

    @Override
      public View onCreateView(LayoutInflater li, ViewGroup container, Bundle icicle)
{
    super.onCreateView(li, container, icicle);
    View v = li.inflate(R.layout.load_layout,container,false);

    FileListAdapter fla = new FileListAdapter(v.getContext(),
            Environment.getExternalStorageDirectory().toString(),
               null,true);

    ListView filelist = (ListView)v.findViewById(R.id.file_list);
        filelist.setAdapter(fla);

    return v;
}

}

package com.awsoft.oscandroid.fragments;

import android.app.Fragment;
import android.support.v7.widget.RecyclerView;

import com.awsoft.oscandroid.views.TrackGroupRecycler;

public class Fragments {

    public static RecyclerView listView;
    public static TrackGroupRecycler trackGroup;
    public static Fragment  fragment, settingsFragment, colorFragment,
                            osdClock, knobCtrl, loadFragment, image_picker,
                            saveFragment, projectDetailsFragment, transparencyFragment;

}

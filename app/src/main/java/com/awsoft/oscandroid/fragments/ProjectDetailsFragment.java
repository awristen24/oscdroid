package com.awsoft.oscandroid.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;

import com.awsoft.oscandroid.MenuItems;
import com.awsoft.oscandroid.R;

import static com.awsoft.oscandroid.OSCDroidMain.*;

public class ProjectDetailsFragment extends Fragment implements Button.OnClickListener{

    public static EditText
            artist_edit,  album_edit, title_edit,
            year_edit,    genre_edit, copyright_edit,
            runtime_edit, date_edit,  label_edit,
            isrc_edit,    upc_edit,   net_edit,  notes_edit;

    public static int PROJECT_DETAILS_ENTRIES = 13;

    public static int ARTIST_INFO   =   0;
    public static int ALBUM_INFO   =    1;
    public static int TITLE_INFO   =    2;
    public static int YEAR_INFO   =     3;
    public static int GENRE_INFO   =    4;
    public static int COPYRIGHT_INFO  = 5;
    public static int RUNTIME_INFO   =  6;
    public static int DATE_INFO   =     7;
    public static int LABEL_INFO    =   8;
    public static int ISRC_INFO   =     9;
    public static int UPC_INFO   =      10;
    public static int NET_INFO  =       11;
    public static int NOTES_INFO   =    12;

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup container, Bundle icicle)
    {
        super.onCreateView(li, container, icicle);
        View v = li.inflate(R.layout.project_details,container,false);

        artist_edit =   (EditText)v.findViewById(R.id.artist_edit);
        album_edit =    (EditText)v.findViewById(R.id.album_edit);
        title_edit =    (EditText)v.findViewById(R.id.title_edit);
        year_edit =     (EditText)v.findViewById(R.id.year_edit);
        genre_edit =    (EditText)v.findViewById(R.id.genre_edit);
        copyright_edit =(EditText)v.findViewById(R.id.copyright_edit);
        runtime_edit =  (EditText)v.findViewById(R.id.runtime_edit);
        date_edit =     (EditText)v.findViewById(R.id.date_edit);
        label_edit =    (EditText)v.findViewById(R.id.label_edit);
        upc_edit =      (EditText)v.findViewById(R.id.upc_edit);
        isrc_edit =     (EditText)v.findViewById(R.id.isrc_edit);
        net_edit =      (EditText)v.findViewById(R.id.net_edit);
        notes_edit =    (EditText)v.findViewById(R.id.notes_edit);

        final ScrollView scroller =      (ScrollView)v.findViewById(R.id.projectScroller);
            notes_edit.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    scroller.requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });

        artist_edit      .setText(projectInfo.get(ARTIST_INFO));
        album_edit       .setText(projectInfo.get(ALBUM_INFO));
        title_edit       .setText(projectInfo.get(TITLE_INFO));
        year_edit        .setText(projectInfo.get(YEAR_INFO));
        genre_edit       .setText(projectInfo.get(GENRE_INFO));
        copyright_edit   .setText(projectInfo.get(COPYRIGHT_INFO));
        runtime_edit     .setText(projectInfo.get(RUNTIME_INFO));
        date_edit        .setText(projectInfo.get(DATE_INFO));
        label_edit       .setText(projectInfo.get(LABEL_INFO));
        isrc_edit        .setText(projectInfo.get(ISRC_INFO));
        upc_edit         .setText(projectInfo.get(UPC_INFO));
        net_edit         .setText(projectInfo.get(NET_INFO));
        notes_edit       .setText(projectInfo.get(NOTES_INFO));

        Button save, discard, cancel;
        save =      (Button)v.findViewById(R.id.save_details_button);
        discard =   (Button)v.findViewById(R.id.discard_details_button);
        cancel =    (Button)v.findViewById(R.id.cancel_details_button);

        save.   setOnClickListener(this);
        discard.setOnClickListener(this);
        cancel. setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {

        if (SettingsFragment.VIBRATE)
            ((Vibrator)v.getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(70);

            switch (v.getId())
            {
                case R.id.save_details_button:

                    projectInfo.set(ARTIST_INFO,    artist_edit .getText().toString());
                    projectInfo.set(ALBUM_INFO,     album_edit  .getText().toString());
                    projectInfo.set(TITLE_INFO,     title_edit  .getText().toString());
                    projectInfo.set(YEAR_INFO,      year_edit   .getText().toString());
                    projectInfo.set(GENRE_INFO,     genre_edit  .getText().toString());
                    projectInfo.set(COPYRIGHT_INFO, copyright_edit.getText().toString());
                    projectInfo.set(RUNTIME_INFO,   runtime_edit.getText().toString());
                    projectInfo.set(DATE_INFO,      date_edit   .getText().toString());
                    projectInfo.set(LABEL_INFO,     label_edit  .getText().toString());
                    projectInfo.set(UPC_INFO,       upc_edit    .getText().toString());
                    projectInfo.set(ISRC_INFO,      isrc_edit   .getText().toString());
                    projectInfo.set(NET_INFO,       net_edit    .getText().toString());
                    projectInfo.set(NOTES_INFO,     notes_edit  .getText().toString());

                    getFragmentManager().popBackStack();
                    MenuItems.doSave(getActivity());

                    break;

                case R.id.discard_details_button:

                    artist_edit      .setText(null);
                    album_edit       .setText(null);
                    title_edit       .setText(null);
                    year_edit        .setText(null);
                    genre_edit       .setText(null);
                    copyright_edit   .setText(null);
                    runtime_edit     .setText(null);
                    date_edit        .setText(null);
                    label_edit       .setText(null);
                    isrc_edit        .setText(null);
                    upc_edit         .setText(null);
                    net_edit         .setText(null);
                    notes_edit       .setText(null);

                    for (int i = 0;i < 13;i++)
                        projectInfo.set(i,null);

                        break;

                case R.id.cancel_details_button:
                    getFragmentManager().beginTransaction().hide(this).commit();
                        bottomControlMenu.setVisibility(View.VISIBLE);
                    break;


            }

    }
}

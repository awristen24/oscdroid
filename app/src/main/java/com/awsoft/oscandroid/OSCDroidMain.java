package com.awsoft.oscandroid;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.graphics.Typeface;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.awsoft.oscandroid.fragments.*;

import static com.awsoft.oscandroid.Listeners.*;
import static com.awsoft.oscandroid.fragments.SettingsFragment.*;
import static com.awsoft.oscandroid.views.GradientView.*;
import static com.awsoft.oscandroid.views.OSDClockView.*;
import static com.awsoft.oscandroid.views.TrackGroupRecycler.*;

import java.io.File;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class OSCDroidMain extends FragmentActivity implements
        TextView.OnEditorActionListener, TextWatcher, View.OnFocusChangeListener{

    public static Rect SCREEN = new Rect();

    public static TextView      valueDisplay;
    public static HorizontalScrollView  displayBar;
    public static LinearLayout  transportArea;

    public static EditText      ch_edit,
                                ctrl_edit,
                                min_edit,
                                max_edit,
                                osctag_edit,
                                projectTitle,
                                projectDescription;

    public static Button        buttonAdd,
                                buttonRemove,
                                buttonNew,
                                buttonLoad,
                                buttonSave,
                                buttonInit;

    public static ImageView     projectDetails;

    public static LinearLayout  bottomControlMenu;
    public static FrameLayout   colorSwatch;
    public static ImageButton   play_button, prev_button, rew_button, ff_button, next_button, record_button, loop_button;

    public static AutoCompleteTextView  actv,
                                        porttext,
                                        trackLabel;

    Vibrator            vibrator;
    SharedPreferences   sp;

    Set<String> ipSet =     new HashSet<>(),
                portSet =   new HashSet<>();

    ArrayAdapter<String>    ipAdapter,
                            portAdapter;

    public static ArrayList<String> projectInfo = new ArrayList<>();

    @Override
    public void onConfigurationChanged(Configuration newconfig)
    {
        super.onConfigurationChanged(newconfig);

        Log.e("OSCDroid", "onConfigurationChanged");

        getWindowManager().
                getDefaultDisplay().
                    getRectSize(SCREEN);

            Fragments.trackGroup.notifyDataSetChanged();

    }

    @Override
    public void onBackPressed() {

        if (VIBRATE) vibrator.vibrate(70);

        bottomControlMenu.setVisibility(View.VISIBLE);

        if (getFragmentManager().getBackStackEntryCount() == 1)
            Toast.makeText(this,"Press back again to exit",Toast.LENGTH_SHORT).show();

        if (getFragmentManager().getBackStackEntryCount() != 0) {
            Log.e("OSCDroid",String.format("Back count : %d",getFragmentManager().getBackStackEntryCount()));
            getFragmentManager().popBackStack();

        }

        else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        File projdir = new File(Environment.getExternalStorageDirectory() + "/OSCDroid");
            projdir.mkdirs();

        for (int i = 0;i < ProjectDetailsFragment.PROJECT_DETAILS_ENTRIES;i++)
            projectInfo.add(null);

        sp = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);

        MenuItems menuItems = new MenuItems(this);

        Typeface lcdfont = Typeface.createFromAsset(getAssets(), "fonts/lcdbold.ttf");

        ch_edit =       (EditText)      findViewById(R.id.channelEdit);
        ctrl_edit =     (EditText)      findViewById(R.id.ctrlEdit);
        max_edit =      (EditText)      findViewById(R.id.maxEdit);
        min_edit =      (EditText)      findViewById(R.id.minEdit);

        osctag_edit =           (EditText)      findViewById(R.id.tagEdit);
        projectTitle =          (EditText)      findViewById(R.id.projectName);
        projectDescription =    (EditText)      findViewById(R.id.projectDescription);
        projectDetails =        (ImageView)     findViewById(R.id.projectDetails);
        colorSwatch =           (FrameLayout)   findViewById(R.id.colorSelect);
        valueDisplay =          (TextView)      findViewById(R.id.valueDisplay);
        displayBar =            (HorizontalScrollView)  findViewById(R.id.dataDisplayBar);
        transportArea =         (LinearLayout)  findViewById(R.id.transportArea);
        bottomControlMenu =     (LinearLayout)  findViewById(R.id.bottomControlMenu);

        actv =          (AutoCompleteTextView)findViewById(R.id.ipAddress);
        porttext =      (AutoCompleteTextView)findViewById(R.id.portNumber);
        trackLabel =    (AutoCompleteTextView)findViewById(R.id.trackLabel);

        ch_edit.     setTypeface(lcdfont);
        ctrl_edit.   setTypeface(lcdfont);
        max_edit.    setTypeface(lcdfont);
        min_edit.    setTypeface(lcdfont);
        valueDisplay.setTypeface(lcdfont);
        actv.        setTypeface(lcdfont);
        porttext.    setTypeface(lcdfont);

        ch_edit.    setOnEditorActionListener(onEditorAction);
        ctrl_edit.  setOnEditorActionListener(onEditorAction);
        min_edit.   setOnEditorActionListener(onEditorAction);
        max_edit.   setOnEditorActionListener(onEditorAction);
        osctag_edit.setOnEditorActionListener(onEditorAction);
        trackLabel. setOnEditorActionListener(onEditorAction);

        buttonAdd =     (Button)findViewById(R.id.buttonAdd);
        buttonRemove =  (Button)findViewById(R.id.buttonRemove);
        buttonLoad =    (Button)findViewById(R.id.buttonLoad);
        buttonSave =    (Button)findViewById(R.id.buttonSave);
        buttonNew =     (Button)findViewById(R.id.buttonNew);
        buttonInit =     (Button)findViewById(R.id.buttonInit);

        buttonAdd.      setOnClickListener(menuItems);
        buttonRemove.   setOnClickListener(menuItems);
        buttonLoad.     setOnClickListener(menuItems);
        buttonSave.     setOnClickListener(menuItems);
        buttonNew.      setOnClickListener(menuItems);
        buttonInit.     setOnClickListener(menuItems);

        ipAdapter =     new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item);
        portAdapter =   new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item);

            ipSet =     sp.getStringSet("iplist",   new HashSet<String>());
            portSet =   sp.getStringSet("portlist", new HashSet<String>());

            ipAdapter.  addAll(ipSet);
            portAdapter.addAll(portSet);

        vibrator = (Vibrator)   getSystemService(VIBRATOR_SERVICE);
        FragmentManager fm = getFragmentManager();

        getWindowManager().
                getDefaultDisplay().
                    getRectSize(SCREEN);

        Paint paint = new Paint();
            paint.setStrokeWidth(1);
            paint.setColor(Color.argb(50, 255, 255, 255));
        paint.setStyle(Paint.Style.STROKE);

        Bitmap bmp = Bitmap.createBitmap(48,48, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bmp);
        canvas.drawRect(0, 0, 45, 45, paint);

        BitmapDrawable bd = new BitmapDrawable(getResources(),bmp);
            colorSwatch.setBackground(bd);

        actv.       setAdapter(ipAdapter);
        porttext.   setAdapter(portAdapter);

        actv.       addTextChangedListener(this);
        porttext.   addTextChangedListener(this);

        actv.       setOnEditorActionListener(this);
        porttext.   setOnEditorActionListener(this);

        actv.       setOnFocusChangeListener(this);
        porttext.   setOnFocusChangeListener(this);

        OSCNetwork.init();

            fm.beginTransaction().addToBackStack(null).commit();

        Fragments.fragment = new MixFragment();
            fm.beginTransaction().
                replace(R.id.tabLayout, Fragments.fragment).
                        commit();

        Fragments.osdClock = new OSDClock();
            fm.beginTransaction().add(android.R.id.content, Fragments.osdClock).
                    hide(Fragments.osdClock).
                    commit();

        Fragments.knobCtrl = new KnobControl();
            fm.beginTransaction().add(R.id.tabLayout,Fragments.knobCtrl).
                    addToBackStack(null).
                        commit();

        readSharedPreferences(this, sp);

    }

    //android:onClick XML
    public void showProjectDetails(View v)
    {
        if (VIBRATE) vibrator.vibrate(70);
        Fragments.projectDetailsFragment = new ProjectDetailsFragment();
            getFragmentManager().beginTransaction().
                    add(android.R.id.content,Fragments.projectDetailsFragment).
                        addToBackStack(null).
                            commit();

        MENU_TOGGLE = false;
            bottomControlMenu.setVisibility(GONE);

    }

    public void showImageSelector(View v)
    {
        if (VIBRATE) vibrator.vibrate(70);

        Fragments.image_picker = new ImageSelectionFragment();
            getFragmentManager().beginTransaction().
                add(R.id.tabLayout, Fragments.image_picker).
                    addToBackStack(null).commit();
    }

    public void showSettings(View v)
    {
        if (VIBRATE) vibrator.vibrate(70);
            Fragments.settingsFragment = new SettingsFragment();
                getFragmentManager().beginTransaction().
                    add(android.R.id.content,Fragments.settingsFragment).
                        addToBackStack(null).commit();

        MENU_TOGGLE = false;
            bottomControlMenu.setVisibility(GONE);

    }

    public void showColorDialog(View v)
    {
        if (VIBRATE) vibrator.vibrate(70);

        if (trackColor.get(SELECTED_INDEX) != 0)
            colorBox_colors[1] = trackColor.get(SELECTED_INDEX);

        Fragments.colorFragment = new ColorFragment();
            getFragmentManager().beginTransaction().
                add(R.id.tabLayout, Fragments.colorFragment).
                    addToBackStack(null).commit();

    }

    public void showClock(View v)
    {
        if (VIBRATE) vibrator.vibrate(70);

        CLOCK_TOGGLE ^= true;
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        if (CLOCK_TOGGLE)   {
            transportArea.removeAllViews();
            transportArea.setGravity(Gravity.CENTER_HORIZONTAL);

            View view = LayoutInflater.from(this).inflate(R.layout.transport,null,false);
                LinearLayout transport = (LinearLayout)view.findViewById(R.id.transportView);

                play_button =       (ImageButton)   view.findViewById(R.id.play_button);
                prev_button =       (ImageButton)   view.findViewById(R.id.prev_button);
                rew_button =        (ImageButton)   view.findViewById(R.id.rew_button);
                ff_button =         (ImageButton)   view.findViewById(R.id.ff_button);
                record_button =     (ImageButton)   view.findViewById(R.id.record_button);
                next_button =       (ImageButton)   view.findViewById(R.id.next_button);
                loop_button =       (ImageButton)   view.findViewById(R.id.loop_button);

                play_button.    setOnClickListener(transportClick);
                play_button.    setOnLongClickListener(transportReset);
                prev_button.    setOnClickListener(transportClick);
                rew_button.     setOnClickListener(transportClick);
                ff_button.      setOnClickListener(transportClick);
                record_button.  setOnClickListener(transportClick);
                next_button.    setOnClickListener(transportClick);
                loop_button.    setOnClickListener(transportClick);

            ft.show(Fragments.osdClock).commit();
            transportArea.addView(transport);
        }
        else {
            transportArea.removeAllViews();
            transportArea.setGravity(Gravity.LEFT);

            transportArea.addView(actv);
            transportArea.addView(porttext);

            ft.hide(Fragments.osdClock).commit();
        }

    }

    public void showControlMenu(View v)
    {
        if (VIBRATE) vibrator.vibrate(70);

        Log.e("OSCDroid",String.format("toggle_panel : %s",MENU_TOGGLE));
        MENU_TOGGLE ^= true;

        if (MENU_TOGGLE)
            bottomControlMenu.setVisibility(View.VISIBLE);
        else
            bottomControlMenu.setVisibility(View.GONE);

        Log.e("OSCDroid", String.format("value : %s", MENU_TOGGLE));

    }
    //======

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

        if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE ||
                event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

            ipSet.  add(actv.getText().toString());
            portSet.add(porttext.getText().toString());

            ipAdapter.  add(actv.getText().toString());
            portAdapter.add(porttext.getText().toString());

            sp.edit().
                    putStringSet("iplist", ipSet).
                        apply();

            sp.edit().
                    putStringSet("portlist", portSet).
                    apply();

            valueDisplay.setText("ip / port settings saved");

        }
        return true;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        try {
            OSCNetwork.IPADDRESS = InetAddress.getByName(actv.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!porttext.getText().toString().isEmpty())
            OSCNetwork.PORT = Integer.parseInt(porttext.getText().toString());

    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        if (!hasFocus) {

            if (!ipSet.isEmpty()) {
                ipSet.add(actv.getText().toString());
                ipAdapter.add(actv.getText().toString());

                sp.edit().
                        putStringSet("iplist", ipSet).
                        apply();

            }

            if (!portSet.isEmpty()) {
                portSet.add(porttext.getText().toString());
                portAdapter.add(porttext.getText().toString());

                sp.edit().
                        putStringSet("portlist", portSet).
                        apply();

            }

            valueDisplay.setText("ip / port settings saved");

        }
    }
}

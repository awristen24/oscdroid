package com.awsoft.oscandroid;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.awsoft.oscandroid.views.OSDClockView;

import static com.awsoft.oscandroid.fragments.Fragments.*;
import static com.awsoft.oscandroid.OSCDroidMain.*;
import static com.awsoft.oscandroid.fragments.SettingsFragment.*;
import static com.awsoft.oscandroid.views.TrackGroupRecycler.*;

public class Listeners {

    private BitmapDrawable  rectangle;
    private Paint           paint;
    private Bitmap          colorbmp;
    private Canvas          canvas;
    private Threads         threads;

    public Listeners(Activity activity)
    {
        threads = new Threads(activity);
    }

    public View.OnClickListener trackEditClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Vibrator vibrator = (Vibrator) v.getContext().getSystemService(VIBRATOR_SERVICE);

            if (VIBRATE) vibrator.vibrate(70);
            Log.e("OSCDroid", String.format("on click - Previous index: %d", SELECTED_INDEX));

            paint = new Paint();
            paint.setStrokeWidth(1);
            paint.setColor(Color.argb(50, 255, 255, 255));
            paint.setStyle(Paint.Style.STROKE);

            colorbmp = Bitmap.createBitmap(48, 48, Bitmap.Config.RGB_565);
            rectangle = new BitmapDrawable(v.getContext().getResources(), colorbmp);

            SELECTED_INDEX = v.getId() - TRACK_GROUP;
            controlType = "track_pane";

            Log.e("OSCDroid", String.format("on click - Selected index: %d", SELECTED_INDEX));

            valueDisplay.  setText(String.format("Selected: %s / Ch: %d / Ctrl: %d / Color: #%08x",
                    trackState.get(SELECTED_INDEX),
                    channelNum.get(SELECTED_INDEX),
                    ctrlNum.get(SELECTED_INDEX),
                    trackColor.get(SELECTED_INDEX)));

            if (trackColor.get(SELECTED_INDEX) != 0) {

                colorbmp.eraseColor(trackColor.get(SELECTED_INDEX));

                canvas = new Canvas(colorbmp);
                canvas.drawRect(0, 0, 45, 45, paint);

                colorSwatch.setBackground(rectangle);

            } else if (trackColor.get(SELECTED_INDEX) == 0) {

                colorbmp.eraseColor(0);

                canvas = new Canvas(colorbmp);
                canvas.drawRect(0, 0, 45, 45, paint);

                colorSwatch.setBackground(rectangle);

            }

            osctag_edit.setText(osctag.get(SELECTED_INDEX));
            trackLabel .setText(trackState.get(SELECTED_INDEX));
            ctrl_edit. setText(String.format("%d", ctrlNum.get(SELECTED_INDEX)));
            ch_edit.   setText(String.format("%d", channelNum.get(SELECTED_INDEX)));
            min_edit.  setText(minimumVal.get(SELECTED_INDEX));
            max_edit.  setText(maximumVal.get(SELECTED_INDEX));

            trackLabel.selectAll();

        }

    };

    public CompoundButton.OnCheckedChangeListener onSoloChecked = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            SELECTED_INDEX = buttonView.getId() - SOLO_GROUP;

            if (isChecked) {
                buttonView.setBackgroundResource(R.drawable.roundrect_solo);
                buttonView.setTextColor(Color.BLACK);
            } else {
                buttonView.setBackgroundResource(R.drawable.roundrect);
                buttonView.setTextColor(Color.WHITE);
            }

            soloState.set(SELECTED_INDEX, isChecked);

        }
    };

    public CompoundButton.OnCheckedChangeListener onMuteChecked = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            SELECTED_INDEX = buttonView.getId() - MUTE_GROUP;

            if (isChecked)  buttonView.setBackgroundResource(R.drawable.roundrect_mute);
            else            buttonView.setBackgroundResource(R.drawable.roundrect);

            muteState.set(SELECTED_INDEX, isChecked);
        }
    };

    public View.OnClickListener onMuteClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Vibrator vibrator = (Vibrator) v.getContext().getSystemService(VIBRATOR_SERVICE);
            if (VIBRATE) vibrator.vibrate(70);

            SELECTED_INDEX = v.getId() - MUTE_GROUP;
            controlType = "mute_button";

            valueDisplay.  setText(String.format("%s / Ch: %d / Ctrl: %d",
                    muteState.get(SELECTED_INDEX) ? "ON" : "OFF", channelNum.get(SELECTED_INDEX), ctrlNum.get(SELECTED_INDEX)));

            OSCNetwork.sendPacket(
                    OSCNetwork.IPADDRESS,
                    OSCNetwork.PORT,
                    muteosctag.get(SELECTED_INDEX),
                    mutectrlNum.get(SELECTED_INDEX),
                    ",iii",
                    booleanToInt(muteState.get(SELECTED_INDEX)),
                    mutectrlNum.get(SELECTED_INDEX),
                    mutechannelNum.get(SELECTED_INDEX),
                    OSCNetwork.PACKET_SIZE
            );

            ctrl_edit. setText(String.format("%d", mutectrlNum.get(SELECTED_INDEX)));
            ch_edit.   setText(String.format("%d", mutechannelNum.get(SELECTED_INDEX)));
            min_edit.  setText(minimumVal.get(SELECTED_INDEX));
            max_edit.  setText(maximumVal.get(SELECTED_INDEX));
            osctag_edit.    setText(muteosctag.get(SELECTED_INDEX));
            trackLabel.setText(trackState.get(SELECTED_INDEX));

        }
    };


    public View.OnClickListener onSoloClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            Vibrator vibrator = (Vibrator) v.getContext().getSystemService(VIBRATOR_SERVICE);
            if (VIBRATE) vibrator.vibrate(70);

            SELECTED_INDEX = v.getId() - SOLO_GROUP;
            controlType = "solo_button";

            valueDisplay.  setText(String.format("%s / Ch: %d / Ctrl: %d",
                            soloState.get(SELECTED_INDEX) ? "ON" : "OFF", channelNum.get(SELECTED_INDEX), ctrlNum.get(SELECTED_INDEX))
            );

            OSCNetwork.sendPacket(
                    OSCNetwork.IPADDRESS,
                    OSCNetwork.PORT,
                    soloosctag.get(SELECTED_INDEX),
                    soloctrlNum.get(SELECTED_INDEX),
                    ",iii",
                    booleanToInt(soloState.get(SELECTED_INDEX)),
                    soloctrlNum.get(SELECTED_INDEX),
                    solochannelNum.get(SELECTED_INDEX),
                    OSCNetwork.PACKET_SIZE
            );

                ctrl_edit.setText(String.format("%d", soloctrlNum.get(SELECTED_INDEX)));
                ch_edit.setText(String.format("%d", solochannelNum.get(SELECTED_INDEX)));
                min_edit.setText(minimumVal.get(SELECTED_INDEX));
                max_edit.setText(maximumVal.get(SELECTED_INDEX));
                osctag_edit.setText(soloosctag.get(SELECTED_INDEX));
                trackLabel.setText(trackState.get(SELECTED_INDEX));

        }

    };

    public SeekBar.OnSeekBarChangeListener seekBarListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progressval, boolean fromUser) {

            if (!fromUser) return;

            Log.e("OSCDroid","onProgressChanged");
            controlType = "fader";

            float min = Float.parseFloat(minimumVal.get(SELECTED_INDEX));
            float inc = (float) progressval / 100;
            float progress = min + inc;

            int data = Float.floatToIntBits(progress);

            ctrlValue.set(SELECTED_INDEX, progressval);
            valueDisplay.  setText(String.format("%.3f / Ch: %d / Ctrl: %d",
                    progress, channelNum.get(SELECTED_INDEX), ctrlNum.get(SELECTED_INDEX)));

            OSCNetwork.sendPacket(
                    OSCNetwork.IPADDRESS,
                    OSCNetwork.PORT,
                    osctag.get(SELECTED_INDEX),
                    ctrlNum.get(SELECTED_INDEX),
                    ",fii",
                    data,
                    ctrlNum.get(SELECTED_INDEX),
                    channelNum.get(SELECTED_INDEX),
                    OSCNetwork.PACKET_SIZE
            );

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

            Vibrator vibrator = (Vibrator) seekBar.getContext().getSystemService(VIBRATOR_SERVICE);
            if (VIBRATE) vibrator.vibrate(70);

            SELECTED_INDEX = seekBar.getId() - FADER_GROUP;
            controlType = "fader";

            ctrl_edit.  setText(String.format("%d", ctrlNum.get(SELECTED_INDEX)));
            ch_edit.    setText(String.format("%d", channelNum.get(SELECTED_INDEX)));
            min_edit.   setText(minimumVal.get(SELECTED_INDEX));
            max_edit.   setText(maximumVal. get(SELECTED_INDEX));
            osctag_edit.setText(osctag.get(SELECTED_INDEX));
            trackLabel.setText(trackState.get(SELECTED_INDEX));

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }

    };

    public View.OnLongClickListener onLongClickListener = new View.OnLongClickListener(){

        @Override
        public boolean onLongClick(View v) {

            Vibrator vibrator = (Vibrator) v.getContext().getSystemService(VIBRATOR_SERVICE);
            if (VIBRATE) vibrator.vibrate(70);

            valueDisplay.  setText("all off");

            new Handler().post(new Runnable() {

                @Override
                public void run() {

                    for (int i = 0; i < muteState.size(); i++) {
                        muteState.set(i, false);
                        soloState.set(i, false);

                        OSCNetwork.sendPacket(
                                OSCNetwork.IPADDRESS,
                                OSCNetwork.PORT,
                                muteosctag.get(i),
                                mutectrlNum.get(i),
                                ",iii",
                                0,
                                mutectrlNum.get(i),
                                mutechannelNum.get(i),
                                OSCNetwork.PACKET_SIZE
                        );

                        OSCNetwork.sendPacket(
                                OSCNetwork.IPADDRESS,
                                OSCNetwork.PORT,
                                soloosctag.get(i),
                                soloctrlNum.get(i),
                                ",iii",
                                0,
                                soloctrlNum.get(i),
                                solochannelNum.get(i),
                                OSCNetwork.PACKET_SIZE
                        );
                    }

                }
            });

            new Handler().post(threads.UpdateThread);

            return true;
        }
    };

    public static TextView.OnEditorActionListener onEditorAction = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT ||
                    event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

                if (LIST_ENTRIES == 0) return false;

                if (controlType.equals("fader") || controlType.equals("knob")) {
                    osctag.set(SELECTED_INDEX, osctag_edit.getText().toString());
                    channelNum.set(SELECTED_INDEX, Integer.parseInt(ch_edit.getText().toString()));
                    ctrlNum.set(SELECTED_INDEX, Integer.parseInt(ctrl_edit.getText().toString()));

                } else if (controlType.equals("track_pane")) {
                    osctag.set(SELECTED_INDEX, osctag_edit.getText().toString());

                    if (!ch_edit.getText().toString().isEmpty() && Integer.parseInt(ch_edit.getText().toString()) > 0)
                        channelNum.set(SELECTED_INDEX, Integer.parseInt(ch_edit.getText().toString()));
                    if (!ctrl_edit.getText().toString().isEmpty() && Integer.parseInt(ctrl_edit.getText().toString()) > 0)
                        ctrlNum.set(SELECTED_INDEX, Integer.parseInt(ctrl_edit.getText().toString()));
                } else if (controlType.equals("mute_button")) {
                    muteosctag.set(SELECTED_INDEX, osctag_edit.getText().toString());

                    if (!ch_edit.getText().toString().isEmpty() && Integer.parseInt(ch_edit.getText().toString()) > 0)
                        mutechannelNum.set(SELECTED_INDEX, Integer.parseInt(ch_edit.getText().toString()));
                    if (!ctrl_edit.getText().toString().isEmpty() && Integer.parseInt(ctrl_edit.getText().toString()) > 0)
                        mutectrlNum.set(SELECTED_INDEX, Integer.parseInt(ctrl_edit.getText().toString()));
                } else if (controlType.equals("solo_button")) {
                    soloosctag.set(SELECTED_INDEX, osctag_edit.getText().toString());

                    if (!ch_edit.getText().toString().isEmpty() && Integer.parseInt(ch_edit.getText().toString()) > 0)
                        solochannelNum.set(SELECTED_INDEX, Integer.parseInt(ch_edit.getText().toString()));
                    if (!ctrl_edit.getText().toString().isEmpty() && Integer.parseInt(ctrl_edit.getText().toString()) > 0)
                        soloctrlNum.set(SELECTED_INDEX, Integer.parseInt(ctrl_edit.getText().toString()));
                }
                else if (controlType.equals("transport")){
                    if (!ch_edit.getText().toString().isEmpty() && Integer.parseInt(ch_edit.getText().toString()) > 0)
                    transportChNum.     set(OSDClockView.TRANSPORT_INDEX, Integer.parseInt(ch_edit.getText().toString()));

                    if (!ctrl_edit.getText().toString().isEmpty() && Integer.parseInt(ctrl_edit.getText().toString()) > 0)
                    transportCtrlNum.   set(OSDClockView.TRANSPORT_INDEX, Integer.parseInt(ctrl_edit.getText().toString()));
                }

                trackState.set(SELECTED_INDEX, trackLabel.getText().toString());
                minimumVal.set(SELECTED_INDEX, min_edit.getText().toString());
                maximumVal.set(SELECTED_INDEX, max_edit.getText().toString());
                valueDisplay.setText("saved values");

                trackGroup.notifyDataSetChanged();

            }

            return false;

        }
    };

}

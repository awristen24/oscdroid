- Landscape layout detection for Effect rack. 30 bands on a HorizontalScrollView. Use different XML.
- mute/solo buttons do not send custom tags "/mute /solo" and those are not defined in the bridge file
- Each track needs to have a longclicklistener which specifies (Remove track, Disable track, Change OSC data/ControlLabel,
Rename track)

- There should be a light/dark UI visibility option
- The data font should be custom
- To quick reset ALL mute/solo buttons, hold any of them for 4-5 seconds
- New should prompt if a layout is not saved
- New will take the value from settings and auto-create that many tracks
- The track creation UI thread is not Asynchronous so it is sort of slow. (Test creation of 1000 tracks)
- The number of tracks cannot clash with the CONTROL_VALUE ids
- There should be an option to display timecode somewhere (HH:MM:SS:mm) or by frames or by SMPTE, bars etc.
- There is no server implementation. It does not receive data, it is to be a control interface.
- If it cannot receive control data, values are not synchronized!
- There should be an option for immersive mode, an option to force portrait or landscape layouts
- The EQ racks look stupid. Need to make them more realistic

- The pads aren't good. They are just rectangles. Pads aren't dynamically created so the sizing is very off.
- The pads should display (Note, Color, glowing, Label)

- The synth is not done...
- Portrait mode, synth should have horizontalscrollview through the keys to expose a full 88key set
- No idea how to lay this one out

- Settings - Theme is very ugly
- App theme - Theme is very ugly
- IP address and port have to be saved
- An onclickListener for the PreferenceFragment should be created

- Since MIDI will be thrown in, I will need to know SYSREQ messages and packing messages together

1) UI fixes, enhancements
2) UI smoothness, speed
3) Immersive mode, fullscreen scaling, screen sizes, orientation
4) Code cleanup
5) PureData relay
6) Dump pureData file from asset
7) Network test, speed, packet size config
8) LCD Display info types
*9)  Can a server be implemented?
10) MIDI functionality and SYSREQ testing, use DAW MIDI monitor
11) Synthesizer layout (2 or 1 row)
12) App obfuscation
13) App release
14) Web page for it
15) Slight monetization?
16) App market
17) App github
18) Other markets
19) Version 2.0

Version 2
- Add x/y pads feature tab sends 2 osc values in a tag. Puredata implement
- DJ layout for landscape?

